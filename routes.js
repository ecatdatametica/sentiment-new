(function () {
    global.tokenData = null;

    app.get("/api/getCombinedFiles", function (req, res) {

        res.send(global.combined);

    });


    /*app.post("api/login",
     passport.authenticate("local", {
     successRedirect: "/loginSuccess",
     failureRedirect: "/loginFailure"
     })
     );

     app.get("/loginSuccess",function()
     {

     });
     app.get("/loginFailure",function()
     {

     });
     */

    app.get("/audio", ensureAuthorization, function (req, resp) {
        log("App Audio File Serv : ");
        var playFile = req.param("playFile");
        /*var token = req.param("token");*/


        var filePath = {};
        filePath.status = false;


        if (playFile != undefined) {
            log("Params File : " + playFile);
            /* log("Requested URL : "+req.url);
             log("Total Audio Files : "+global.list.audio.length);*/
            var i = 0;
            while (i != global.list.audio.length) {
                if (playFile == global.list.audio[i]) {
                    log("File Found : " + playFile);
                    //get files location
                    log(app.get("audioPath") + playFile);
                    filePath.status = true;

                    var filePath = app.get("audioPath") + playFile;
                    log("FILE PATH : " + filePath);

                    var stat = fs.statSync(filePath);
                    log(stat);
                    log(stat.size);
                    resp.writeHead(200, {
                        'Content-Type': 'audio/mpeg',
                        'Content-Length': stat.size
                    });

                    var readStream = fs.createReadStream(filePath);
                    // We replaced all the event handlers with a simple call to readStream.pipe()
                    log("Streaming.......");
                    readStream.pipe(resp);
                }
                else {
                    log("Requested Resource Not Found");


                }
                i += 1;
            }
            //readStream  = fs.createReadStream(app.get("audioPath"));
        }
        else {
            log("Requested Params Not Found");
            resp.send(filePath);
        }

    });


    /*Audio Text */
    app.get("/api/audioText", ensureAuthorization, function (req, resp) {
        log("App Audio Text File Serv : ");
        var playFile = req.param("playFile");
        var filePath = {};
        filePath.status = false;


        if (playFile != undefined) {
            log("Params File : " + playFile);
            /* log("Requested URL : "+req.url);
             log("Total Audio Files : "+global.list.audio.length);*/
            var i = 0;
            while (i != global.list.audioText.length) {
                if (playFile == global.list.audioText[i]) {
                    log("File Found : " + playFile);
                    //get files location
                    log(app.get("audioText") + playFile);
                    filePath.status = true;

                    var filePath = app.get("audioText") + playFile;

                    fs.readFile(filePath, "utf8", function (error, data) {
                        log(data);
                        resp.send(data);
                    });


                }
                else {
                    log("Requested Resource Not Found");


                }
                i += 1;
            }
            //readStream  = fs.createReadStream(app.get("audioPath"));
        }
        else {
            log("Requested Params Not Found");
            resp.send(filePath);
        }
    });


    app.get("/api/callDriver", ensureAuthorization, function (req, res) {
        var sent = {};
        sent.status = false;
        var startDate = req.param('startDate');
        var endDate = req.param('endDate');

        if (user.isDB) {
            var options = {};
            options.values = [];
            log('startDate',startDate);
            log('endDate',endDate);
            options.values.push(startDate);
            options.values.push(endDate);
            options.timeout = 4000;
            options.sql = "select primary_topic, secondary_topic, modelled_topic, count(*) as count from topics_data where file_ts >= ? and file_ts <= ? group by primary_topic, secondary_topic, modelled_topic";
            connection.query(options, function (err, rows, fields) {
                if (!err) {
                    log('CallDriverSql',options.sql);
                    if (rows.length != 0) {
                        sent.status = true;
                        //sent.data = rows;
                        var temp = [];
                        temp = rows;
                        sent.data = utility.performModelling(temp)
                        log(rows.length);
                    }
                    res.send(sent);
                }

            });
        }
        else {
            log("DataBase Connection Issue");
            res.send(sent);
        }

    });


    app.get("/", function (req, resp) {
        log("Starting App Index.html");
        log(req.url);
        resp.pipe(__dirname + '/PUBLIC/index.html');

    });


    app.get('/api/sentiment', ensureAuthorization, function (req, res) {

        var startDate = req.param('startDate');
        var endDate = req.param('endDate');


        if (user.isDB) {

            var sent = {};
            sent.status = false;
            if (user.isDB) {


                var startValid = moment(startDate).isValid();
                var endValid = moment(endDate).isValid();
                log(startValid + " " + endValid);

                if (startValid && endValid) {

                    /*Another Table
                     sentiments_data
                     */

                    var options = {};
                    options.values = [];
                    options.values.push(startDate);
                    options.values.push(endDate);
                    options.sql = 'SELECT * from sentiments_data where file_ts >= ? AND file_ts <= ? order by file_ts';
                    options.timeout = 4000;


                    connection.query(options, function (err, rows, fields) {
                        if (!err) {
                            log("Data row received For Query");
                            log("Data row Length : " + rows.length);
                            log("Executing Query :");
                            log("StartDate : " + startDate);
                            log("EndDate : " + endDate);
                            log(options.sql);


                            if (rows.length != 0) {
                                sent.status = true;
                                sent.data = rows;
                            }
                            res.send(sent);
                        }
                    });

                }
                else {
                    log("Invalid Date Format/Or Data Error");
                }

            }
            else {
                log("DataBase Connection Issue");
                res.send(sent);
            }

        }
        else {
            var dataSize = parseFloat(Math.floor(Math.random() * 1000) + 100);
            log("DB Error.No Connection Estabilished");
            var ds = dataGenerator(startDate, endDate, dataSize);
            log("Sending JSON Simulated Data...");
            res.send(ds);
        }

    });


    app.get('/api/yearly', ensureAuthorization, function (req, res) {

        var startDate = req.param('startDate');
        var endDate = req.param('endDate');


        if (user.isDB) {

            var sent = {};
            sent.status = false;
            if (user.isDB) {


                var startValid = moment(startDate).isValid();
                var endValid = moment(endDate).isValid();
                log(startValid + " " + endValid);

                if (startValid && endValid) {

                    /*Another Table
                     sentiments_data
                     */

                    var options = {};
                    options.values = [];
                    options.values.push(startDate);
                    options.values.push(endDate);
                    options.sql = 'select conv_id,rep_id,dept_id,file_ts,year(file_ts) YR, MONTH(file_ts) as MNTH, WEEK(file_ts) as WK,DAY(file_ts) as DD, count(*) record_count,avg(polpospercent)*100,avg(polnegpercent)*100,avg(Anger),avg(Surprise),avg(Sadness),avg(Joy),avg(Disgust),avg(Fear),avg(Trust),avg(Anticipation) from sentiments_data where file_ts >= ? AND file_ts <= ? group by YR,MNTH,WK,DD order by YR,MNTH,WK,DD';
                    options.timeout = 4000;
                    log("Start Date : " + startDate);
                    log("End   Date : " + endDate);
                    log("SQL QUERY  :");
                    log(options.sql);

                    connection.query(options, function (err, rows, fields) {
                        if (!err) {
                            log("Data row received For Query");
                            log("Data row Length : " + rows.length);
                            log("Executing Query :");
                            log("StartDate : " + startDate);
                            log("EndDate : " + endDate);
                            log(options.sql);


                            if (rows.length != 0) {
                                sent.status = true;
                                sent.data = rows;
                            }
                            res.send(sent);
                        }
                    });

                }
                else {
                    log("Invalid Date Format/Or Data Error");
                }

            }
            else {
                log("DataBase Connection Issue");
                res.send(sent);
            }

        }


    });


    /*CORE API For NEW DASHBOARD*/


    app.get('/api/customDropDown', ensureAuthorization, function (req, res) {
        var anyFilter = false;
        var startDate = req.param('startDate');
        var endDate = req.param('endDate');
        var dept_id = req.param('dept_id');
        var category_id = req.param('category_id');
        var rep_id = req.param('rep_id');
        var channel = req.param('channel');


        if (user.isDB) {

            var sent = {};
            sent.status = false;
            if (user.isDB) {

                var startValid = moment(startDate).isValid();
                var endValid = moment(endDate).isValid();
                log(startValid + " " + endValid);

                if (startValid && endValid) {

                    /*Another Table
                     sentiments_data
                     */
                    var options = {};
                    options.values = [];

                    options.values.push(startDate);
                    options.values.push(endDate);


                    append = "select conv_id,rep_id,dept_id,category_id,channel,date_format(file_ts,'%m/%d/%Y %h:00:00 %p') file_time,year(file_ts) YR, MONTH(file_ts) as MNTH, WEEK(file_ts) as WK, DAY(file_ts) as DD,HOUR(file_ts) as HR,polpospercent*100 as polpospercent,polnegpercent*100 as polnegpercent,Anger,Surprise,Sadness,Joy,Disgust,Fear,Trust,Anticipation from sentimental_data where file_ts >= ? AND file_ts <= ? "

                    if (dept_id != "Filter By Department") {
                        append += 'AND dept_id = ? ';

                        options.values.push(dept_id);
                        anyFilter = true;
                    }

                    if (category_id != "Filter By Category") {
                        append += 'AND category_id = ? ';
                        options.values.push(category_id);
                        anyFilter = true;
                    }

                    if (rep_id != "Filter By Representative") {
                        append += 'AND rep_id = ? ';
                        options.values.push(rep_id);
                        anyFilter = true;
                    }

                    if (channel != "Filter By Channel") {
                        append += 'AND channel = ? ';
                        options.values.push(channel);
                        anyFilter = true;
                    }


                    append += " order by file_ts";

                    log("Sequel Statement Test");

                    //  sentiments_data
                    options.timeout = 4000;
                    log("Start Date : " + startDate);
                    log("End   Date : " + endDate);
                    log("SQL QUERY  :");
                    options.sql = append;

                    if (anyFilter) {

                        connection.query(options, function (err, rows, fields) {
                            if (!err) {
                                log("Data row received For Query");
                                log("Data row Length : " + rows.length);
                                log("Executing Query :");
                                log("StartDate : " + startDate);
                                log("EndDate : " + endDate);
                                log("Dept_ID : " + dept_id);
                                log(options.sql);


                                if (rows.length != 0) {
                                    sent.status = true;
                                    sent.data = rows;
                                    log(rows.length);
                                }
                                res.send(sent);
                            }
                        });

                    }
                    else {
                        log("No SQL Statement Formed");
                        res.send(sent);
                    }

                }
                else {
                    log("Invalid Date Format/Or Data Error");
                }

            }
            else {
                log("DataBase Connection Issue");
                res.send(sent);
            }

        }


    });


// Get Date For Custom Based On DropDowen Params
    app.get('/api/custom', ensureAuthorization, function (req, res) {
        var anyFilter = false;
        var startDate = req.param('startDate');
        var endDate = req.param('endDate');
        var dept_id = req.param('dept_id');
        var category_id = req.param('category_id');
        var rep_id = req.param('rep_id');
        var channel = req.param('channel');


        if (user.isDB) {

            var sent = {};
            sent.status = false;
            if (user.isDB) {

                var startValid = moment(startDate).isValid();
                var endValid = moment(endDate).isValid();
                log(startValid + " " + endValid);

                if (startValid && endValid) {

                    /*Another Table
                     sentiments_data
                     */
                    var options = {};
                    options.values = [];

                    options.values.push(startDate);
                    options.values.push(endDate);


                    append = "select conv_id,rep_id,dept_id,category_id,channel,date_format(file_ts,'%m/%d/%Y %h:00:00 %p') file_time,year(file_ts) YR, MONTH(file_ts) as MNTH, WEEK(file_ts) as WK, DAY(file_ts) as DD,HOUR(file_ts) as HR,count(*) record_count,avg(polpospercent)*100 as polpospercent,avg(polnegpercent)*100 as polnegpercent,avg(Anger) as Anger,avg(Surprise) as Surprise,avg(Sadness) as Sadness,avg(Joy) as Joy,avg(Disgust) as Disgust,avg(Fear) as Fear,avg(Trust) as Trust,avg(Anticipation) as Anticipation from sentimental_data where file_ts >= ? AND file_ts <= ? "

                    if (dept_id != "Filter By Department") {
                        append += 'AND dept_id = ? ';

                        options.values.push(dept_id);
                        anyFilter = true;
                    }

                    if (category_id != "Filter By Category") {
                        append += 'AND category_id = ? ';
                        options.values.push(category_id);
                        anyFilter = true;
                    }

                    if (rep_id != "Filter By Representative") {
                        append += 'AND rep_id = ? ';
                        options.values.push(rep_id);
                        anyFilter = true;
                    }

                    if (channel != "Filter By Channel") {
                        append += 'AND channel = ? ';
                        options.values.push(channel);
                        anyFilter = true;
                    }


                    append += " group by YR,MNTH,WK,DD,HR order by YR,MNTH,WK,DD,HR,file_ts";

                    log("Sequel Statement Test");
                    /*
                     var sql1 = "select conv_id,rep_id,dept_id,category_id,channel,file_ts,year(file_ts) YR, MONTH(file_ts) as MNTH, WEEK(file_ts) as WK, DAY(file_ts) as DD,HOUR(file_ts) as HR,count(*) record_count,avg(polpospercent) as polpospercent,avg(polnegpercent) as polnegpercent,avg(Anger) as Anger,avg(Surprise) as Surprise,avg(Sadness) as Sadness,avg(Joy) as Joy,avg(Disgust) as Disgust,avg(Fear) as Fear,avg(Trust) as Trust,avg(Anticipation) as Anticipation from sentimental_data where file_ts >= ? AND file_ts <= ?  and dept_id = ? and category_id = ?  and  rep_id = ?  and channel = ? group by YR,MNTH,WK,DD,HR order by YR,MNTH,WK,DD,HR,file_ts";*/


                    //  sentiments_data
                    options.timeout = 4000;
                    log("Start Date : " + startDate);
                    log("End   Date : " + endDate);
                    log("SQL QUERY  :");
                    options.sql = append;

                    if (anyFilter) {

                        connection.query(options, function (err, rows, fields) {
                            if (!err) {
                                log("Data row received For Query");
                                log("Data row Length : " + rows.length);
                                log("Executing Query :");
                                log("StartDate : " + startDate);
                                log("EndDate : " + endDate);
                                log("Dept_ID : " + dept_id);
                                log(options.sql);


                                if (rows.length != 0) {
                                    sent.status = true;
                                    sent.data = rows;
                                    log(rows.length);
                                }
                                res.send(sent);
                            }
                        });

                    }
                    else {
                        log("No SQL Statement Formed");
                        res.send(sent);
                    }

                }
                else {
                    log("Invalid Date Format/Or Data Error");
                }

            }
            else {
                log("DataBase Connection Issue");
                res.send(sent);
            }

        }


    });


    app.get('/api/hourly', ensureAuthorization, function (req, res) {

        var startDate = req.param('startDate');
        var endDate = req.param('endDate');
        var dept_id = req.param('dept_id');
        var type = req.param('type');

        log("Query Type : " + type);

        if (user.isDB) {

            var sent = {};
            sent.status = false;
            if (user.isDB) {


                var startValid = moment(startDate).isValid();
                var endValid = moment(endDate).isValid();
                log(startValid + " " + endValid);

                if (startValid && endValid) {

                    /*Another Table
                     sentiments_data
                     */

                    var options = {};
                    options.values = [];

                    if (dept_id == null || dept_id == undefined) {
                        options.values.push(startDate);
                        options.values.push(endDate);

                    }
                    else {

                        options.values.push(startDate);
                        options.values.push(endDate);
                        options.values.push(dept_id);
                    }

                    if (dept_id == 'table') {
                        log("Param Detected");
                        options.sql = "select conv_id,rep_id,dept_id,category_id,channel,file_ts as file_time,year(file_ts) YR, MONTH(file_ts) as MNTH, WEEK(file_ts) as WK, DAY(file_ts) as DD,HOUR(file_ts) as HR,polpospercent*100 as polpospercent,polnegpercent*100 as polnegpercent,Anger,Surprise,Sadness,Joy,Disgust,Fear,Trust,Anticipation from sentimental_data where file_ts >= ? AND file_ts <= ? order by file_time";
                    } else {
                        log("dept_id Param Not Detected");
                        if (type == "yesterday") {

                            options.sql = "select conv_id,rep_id,dept_id,category_id,channel,date_format(file_ts,'%m/%d/%Y %h:00:00 %p') file_time,year(file_ts) YR, MONTH(file_ts) as MNTH, WEEK(file_ts) as WK, DAY(file_ts) as DD,HOUR(file_ts) as HR,count(*) record_count,avg(polpospercent)*100 as polpospercent,avg(polnegpercent)*100 as polnegpercent,avg(Anger) as Anger,avg(Surprise) as Surprise,avg(Sadness) as Sadness,avg(Joy) as Joy,avg(Disgust) as Disgust,avg(Fear) as Fear,avg(Trust) as Trust,avg(Anticipation) as Anticipation from sentimental_data where file_ts >= ? AND file_ts <= ? group by file_time";
                        }
                        else if (type == "week") {
                            options.sql = "select conv_id,rep_id,dept_id,category_id,channel,date_format(file_ts,'%m/%d/%Y') file_time,year(file_ts) YR, MONTH(file_ts) as MNTH, WEEK(file_ts) as WK, DAY(file_ts) as DD,HOUR(file_ts) as HR,count(*) record_count,avg(polpospercent)*100 as polpospercent,avg(polnegpercent)*100 as polnegpercent,avg(Anger) as Anger,avg(Surprise) as Surprise,avg(Sadness) as Sadness,avg(Joy) as Joy,avg(Disgust) as Disgust,avg(Fear) as Fear,avg(Trust) as Trust,avg(Anticipation) as Anticipation from sentimental_data where file_ts >= ? AND file_ts <= ? group by file_time";


                        }
                        else if (type == "month") {
                            options.sql = "select conv_id,rep_id,dept_id,category_id,channel,date_format(file_ts,'%m/%d/%Y') file_time,year(file_ts) YR, MONTH(file_ts) as MNTH, WEEK(file_ts) as WK, DAY(file_ts) as DD,HOUR(file_ts) as HR,count(*) record_count,avg(polpospercent)*100 as polpospercent,avg(polnegpercent)*100 as polnegpercent,avg(Anger) as Anger,avg(Surprise) as Surprise,avg(Sadness) as Sadness,avg(Joy) as Joy,avg(Disgust) as Disgust,avg(Fear) as Fear,avg(Trust) as Trust,avg(Anticipation) as Anticipation from sentimental_data where file_ts >= ? AND file_ts <= ? group by file_time";

                        }
                        else if (type == "year") {

                            options.sql = "select conv_id,rep_id,dept_id,category_id,channel,date_format(file_ts,'%m/01/%Y') file_time,year(file_ts) YR, MONTH(file_ts) as MNTH, WEEK(file_ts) as WK, DAY(file_ts) as DD,HOUR(file_ts) as HR,count(*) record_count,avg(polpospercent)*100 as polpospercent,avg(polnegpercent)*100 as polnegpercent,avg(Anger) as Anger,avg(Surprise) as Surprise,avg(Sadness) as Sadness,avg(Joy) as Joy,avg(Disgust) as Disgust,avg(Fear) as Fear,avg(Trust) as Trust,avg(Anticipation) as Anticipation from sentimental_data where file_ts >= ? AND file_ts <= ? group by file_time";

                        }  //child if else ends
                    }  //if else ends


                    //  sentiments_data
                    options.timeout = 4000;
                    log("Start Date : " + startDate);
                    log("End   Date : " + endDate);
                    log("SQL QUERY  :");

                    connection.query(options, function (err, rows, fields) {
                        if (!err) {
                            log("Data row received For Query");
                            log("Data row Length : " + rows.length);
                            log("Executing Query :");
                            log("StartDate : " + startDate);
                            log("EndDate : " + endDate);
                            log("Dept_ID : " + dept_id);
                            log(options.sql);


                            if (rows.length != 0) {
                                sent.status = true;
                                sent.data = rows;
                            }
                            res.send(sent);
                        }
                    });

                }
                else {
                    log("Invalid Date Format/Or Data Error");
                }

            }
            else {
                log("DataBase Connection Issue");
                res.send(sent);
            }

        }


    });


    app.get('/api/topic', ensureAuthorization, function (req, res) {

        var startDate = req.param('startDate');
        var endDate = req.param('endDate');


        if (user.isDB) {

            var sent = {};
            sent.status = false;
            if (user.isDB) {


                var startValid = moment(startDate).isValid();
                var endValid = moment(endDate).isValid();
                log(startValid + " " + endValid);

                if (startValid && endValid) {

                    /*Another Table
                     sentiments_data
                     */

                    var options = {};
                    options.values = [];
                    options.values.push(startDate);
                    options.values.push(endDate);
                    options.sql = 'SELECT * from topics_data where file_ts >= ? AND file_ts <= ? order by file_ts';
                    options.timeout = 4000;


                    connection.query(options, function (err, rows, fields) {
                        if (!err) {
                            log("Data row received For Query");
                            log("Data row Length : " + rows.length);
                            log("Executing Query :");
                            log("StartDate : " + startDate);
                            log("EndDate : " + endDate);
                            log(options.sql);


                            if (rows.length != 0) {
                                sent.status = true;
                                sent.data = rows;
                            }
                            res.send(sent);
                        }
                    });

                }
                else {
                    log("Invalid Date Format/Or Data Error");
                }

            }
            else {
                log("DataBase Connection Issue");
                res.send(sent);
            }

        }
        else {
            var dataSize = parseFloat(Math.floor(Math.random() * 1000) + 100);
            log("DB Error.No Connection Estabilished");
            var ds = dataGeneratorTopic(startDate, endDate, dataSize);
            log("Sending JSON Simulated Data...");
            res.send(ds);
        }

    });


    /*http://localhost:3000/api/test?startDate='2015-12-10'&endDate='2015-12-11'&dataSize=1000*/
    /*localhost:3000/api/testData?startDate=2015-12-10&endDate=2015-12-11&dataSize=1000*/
    /*http://localhost:3000/api/sentiment*/


    app.get("/api/testData", ensureAuthorization, function (req, res) {
        var sent = {};
        sent.status = false;

        var startDate = req.param('startDate');
        var endDate = req.param('endDate');
        var dataSize = req.param('dataSize');

        var startValid = moment(startDate).isValid();
        var endValid = moment(endDate).isValid();

        if (startValid && endValid) {


            //detect if File Exist
            log("Test Data Generator For Sentiment Analysis");
            log("Start Date : " + startDate);
            log("End   Date : " + endDate);
            log("DataSize   : " + dataSize);

            var i = 0;
            var List = [];
            while (i != parseInt(dataSize)) {
                tuple = {};
                tuple.Anger = parseFloat(Math.floor(Math.random() * 100) + 0.5);
                tuple.Anticipation = parseFloat(Math.floor(Math.random() * 100) + 0.5);
                tuple.Disgust = parseFloat(Math.floor(Math.random() * 100) + 0.5);
                tuple.Fear = parseFloat(Math.floor(Math.random() * 100) + 0.5);
                tuple.Joy = parseFloat(Math.floor(Math.random() * 100) + 0.5);
                tuple.Sadness = parseFloat(Math.floor(Math.random() * 100) + 0.5);
                tuple.Surprise = parseFloat(Math.floor(Math.random() * 100) + 0.5);
                tuple.Trust = parseFloat(Math.floor(Math.random() * 100) + 0.5);
                tuple.conv_id = parseFloat(Math.floor(Math.random() * 100) + 0.5);
                tuple.dept_id = parseFloat(Math.floor(Math.random() * 100) + 0.5);
                tuple.file_ts = randomTime(new Date(startDate), new Date(endDate));
                tuple.polnegpercent = parseFloat(Math.floor(Math.random() * 100) + 0.5);
                tuple.polpospercent = parseFloat(Math.floor(Math.random() * 100) + 0.5);
                tuple.processed_date = randomTime(new Date(startDate), new Date(endDate));  //Date
                tuple.rep_id = parseFloat(Math.floor(Math.random() * 1000) + 1);
                List.push(tuple);

                i += 1;
            }
            sent.status = true;
            sent.data = List;

        }
        else {
            log("Date Format Not Valid");
        }

        res.send(sent);

    });


    /*CORE FOR SENTIMENT FOR LANDING PAGE*/
    app.get('/api/totalDataChat', ensureAuthorization, function (req, res) {
        var sent = {};
        sent.status = false;


        if (user.isDB) {
            var options = {};
            options.timeout = 4000;
            options.sql = "select avg(polpospercent) as polpospercent,avg(polnegpercent) as polnegpercent,avg(Anger) as Anger,avg(Surprise) as Surprise,avg(Sadness) as Sadness,avg(Joy) as Joy,avg(Disgust) as Disgust,avg(Fear) as Fear,avg(Trust) as Trust,avg(Anticipation) as Anticipation ,count(*) total_count from sentimental_data where channel = 'Chat'";

            connection.query(options, function (err, rows, fields) {
                if (!err) {
                    log(options.sql);
                    if (rows.length != 0) {
                        sent.status = true;
                        sent.data = rows;
                        log(rows.length);
                    }
                    res.send(sent);
                }

            });


        }
        else {
            log("DataBase Connection Issue");
            res.send(sent);
        }
    });


    app.get('/api/totalDataEmail', ensureAuthorization, function (req, res) {
        var sent = {};
        sent.status = false;


        if (user.isDB) {
            var options = {};
            options.timeout = 4000;
            options.sql = "select avg(polpospercent) as polpospercent,avg(polnegpercent) as polnegpercent,avg(Anger) as Anger,avg(Surprise) as Surprise,avg(Sadness) as Sadness,avg(Joy) as Joy,avg(Disgust) as Disgust,avg(Fear) as Fear,avg(Trust) as Trust,avg(Anticipation) as Anticipation,count(*) total_count from sentimental_data where channel = 'Email'";

            connection.query(options, function (err, rows, fields) {
                if (!err) {
                    log(options.sql);
                    if (rows.length != 0) {
                        sent.status = true;
                        sent.data = rows;
                        log(rows.length);
                    }
                    res.send(sent);
                }

            });


        }
        else {
            log("DataBase Connection Issue");
            res.send(sent);
        }
    });


    app.get('/api/totalDataVoice', ensureAuthorization, function (req, res) {
        var sent = {};
        sent.status = false;


        if (user.isDB) {
            var options = {};
            options.timeout = 4000;
            options.sql = "select avg(polpospercent) as polpospercent,avg(polnegpercent) as polnegpercent,avg(Anger) as Anger,avg(Surprise) as Surprise,avg(Sadness) as Sadness,avg(Joy) as Joy,avg(Disgust) as Disgust,avg(Fear) as Fear,avg(Trust) as Trust,avg(Anticipation) as Anticipation ,count(*) total_count from sentimental_data where channel = 'Voice'";

            connection.query(options, function (err, rows, fields) {
                if (!err) {
                    log(options.sql);
                    if (rows.length != 0) {
                        sent.status = true;
                        sent.data = rows;
                        log(rows.length);
                    }
                    res.send(sent);
                }

            });


        }
        else {
            log("DataBase Connection Issue");
            res.send(sent);
        }
    });


    app.get('/api/WeekDayWise', ensureAuthorization, function (req, res) {
        var sent = {};
        sent.status = false;


        if (user.isDB) {
            var options = {};
            options.timeout = 4000;
            options.sql = "SELECT sentiments, MAX(CASE WHEN unit = 'Monday' THEN value END) `Monday`, MAX(CASE WHEN unit = 'Tuesday' THEN value END) `Tuesday`, MAX(CASE WHEN unit = 'Wednesday' THEN value END) `Wednesday`, MAX(CASE WHEN unit = 'Thursday' THEN value END) `Thursday`, MAX(CASE WHEN unit = 'Friday' THEN value END) `Friday`, MAX(CASE WHEN unit = 'Saturday' THEN value END) `Saturday`, MAX(CASE WHEN unit = 'Sunday' THEN value END) `Sunday` FROM ( SELECT unit, sentiments, CASE sentiments WHEN 'Anger' THEN Anger WHEN 'Surprise' THEN Surprise WHEN 'Sadness' THEN Sadness WHEN 'Joy' THEN Joy WHEN 'Disgust' THEN Disgust WHEN 'Fear' THEN Fear WHEN 'Trust' THEN Trust WHEN 'Anticipation' THEN Anticipation END value FROM (select DAYNAME(file_ts) as unit, avg(Anger) as Anger,avg(Surprise) as Surprise,avg(Sadness) as Sadness,avg(Joy) as Joy,avg(Disgust) as Disgust, avg(Fear) as Fear,avg(Trust) as Trust,avg(Anticipation) as Anticipation from sentimental_data group by unit) sentiment_data_test CROSS JOIN ( SELECT 'Anger' sentiments UNION ALL SELECT 'Surprise' UNION ALL SELECT 'Sadness' UNION ALL SELECT 'Joy' UNION ALL SELECT 'Disgust' UNION ALL SELECT 'Fear' UNION ALL SELECT 'Trust' UNION ALL SELECT 'Anticipation' ) c ) q GROUP BY sentiments ORDER BY FIELD(sentiments, 'Anger', 'Surprise', 'Sadness', 'Joy', 'Disgust', 'Fear', 'Trust', 'Anticipation')";


            connection.query(options, function (err, rows, fields) {
                if (!err) {
                    log(options.sql);
                    if (rows.length != 0) {
                        sent.status = true;
                        sent.data = rows;
                        log(rows.length);
                    }
                    res.send(sent);
                }

            });


        }
        else {
            log("DataBase Connection Issue");
            res.send(sent);
        }
    });


    app.get('/api/totalData', ensureAuthorization, function (req, res) {
        var sent = {};
        sent.status = false;


        if (user.isDB) {
            var options = {};
            options.timeout = 4000;
            options.sql = "select avg(polpospercent) as polpospercent,avg(polnegpercent) as polnegpercent,avg(Anger) as Anger,avg(Surprise) as Surprise,avg(Sadness) as Sadness,avg(Joy) as Joy,avg(Disgust) as Disgust,avg(Fear) as Fear,avg(Trust) as Trust,avg(Anticipation) as Anticipation,count(*) total_count from sentimental_data";


            connection.query(options, function (err, rows, fields) {
                if (!err) {
                    log(options.sql);
                    if (rows.length != 0) {
                        sent.status = true;
                        sent.data = rows;
                        log(rows.length);
                    }
                    res.send(sent);
                }

            });


        }
        else {
            log("DataBase Connection Issue");
            res.send(sent);
        }
    });


    app.post("/api/auth", function (req, resp) {
        var sent = {};
        sent.status = false;
        sent.authenticated = false;
        var username = req.body.username || req.param('username');
        var password = req.body.password || req.param('password');

        if (username == "kohls" && password == "kohls") {
            sent.status = true;
            sent.authenticated = true;
            log("Authenticated");
            global.tokenData = jwt.sign(user, app.get('superSecret'), {
                expiresInMinutes: 1440 // expires in 24 hours
            });
            sent.token = tokenData;
        }
        else {
            log("Invalid Credentials");
            /*log("Username : "+username);
             log("Password : "+password);*/
        }
        resp.send(sent);

    });

   function myReadFile(file, fileToSend, searchText, getAudioFile){
       fs.readFileSync(file, "utf8", function (error, data) {
           log("Reading File");
           log("AUDIO FILE : " + getAudioFile);

           if (data.indexOf(searchText) != -1) {
               log("Required File Found Which Contains " + searchText);
               log('length of filetosend is ',fileToSend.length )

               return getAudioFile;
           }
           else {
               log("Current File Does Not Contain The Require Text");

               return null;
           }


       });
   }
    app.get('/api/audioTextSentimentsSearch', ensureAuthorization, function (req, res) {
        var sentAudio = {};
        sentAudio.status = false;
        var searchText = req.body.username || req.param('searchText');
        if (user.isDB) {
            //Perform Search On text And Filtyer Out Files
            var fileToSend = {
                'audio':[],
                'audioText':[]
            };
            var i = 0;
            var refinedObject = {}

            var audioTextObj = global.combined.audioText
            var audioFileObj = global.combined.audio
            for(var j= 0; j< audioTextObj.length ; j++){
                refinedObject[audioTextObj[j]] = audioFileObj[j]
            }
            Object.keys(refinedObject).forEach(function (audiotext){
               var getFile = app.get("audioText") + audiotext;

               var getAudioFile = refinedObject.audiotext;

               // myReadFile(getFile, fileToSend, searchText, getAudioFile);
                var filereturned =fs.readFileSync(getFile).toString().toLowerCase();
                //log('returned file is ', filereturned)
                if(filereturned  != null){
                    if (filereturned.indexOf(searchText) != -1) {
                        log("Required File Found Which Contains " + searchText);
                        fileToSend.audio.push(refinedObject[audiotext]);
                        fileToSend.audioText.push(audiotext);
                        log('length of filetosend is ',fileToSend.length )

                    }
                    else {
                        log("Current File Does Not Contain The Require Text");
                        /* if(i==global.combined.audioText.length){
                         res.send(fileToSend);
                         //break;
                         }*/
                        //return null;
                    }
                }
               /*var filereturned =fs.readFileSync(getFile, "utf8", function (error, data) {
                   log("Reading File");
                   log("AUDIO FILE : " + getAudioFile);

                   if (data.indexOf(searchText) != -1) {
                       log("Required File Found Which Contains " + searchText);
                        fileToSend.push(getAudioFile);
                       log('length of filetosend is ',fileToSend.length )
                       //log('i value is' , i);
                       *//*if(i==global.combined.audioText.length){
                        res.send(fileToSend);
                        //break;
                        }*//*
                      // return getAudioFile;
                   }
                   else {
                       log("Current File Does Not Contain The Require Text");
                       *//* if(i==global.combined.audioText.length){
                        res.send(fileToSend);
                        //break;
                        }*//*
                       //return null;
                   }


               });*/
              /* if (filereturned != null){
                   fileToSend.push(filereturned);
               }*/
               log('length of filetosend ', fileToSend.length);
               i++;
               if(i == global.combined.audioText.length){
                   res.send(fileToSend);
               }
           });
           /* while (i < global.combined.audioText.length) {
                var getFile = app.get("audioText") + global.combined.audioText[i];
                var getAudioFile = global.combined.audio[i];
                log('file is ' , getFile);
                myReadFile(getFile, fileToSend, searchText, getAudioFile);
                log('length of filetosend ', fileToSend.length)
                log('value of i before incremneting ', i);
                i += 1;
            } //loop end
            res.send(fileToSend);*/
            //res.send(fileToSend);
        }
        else {
            log("DataBase Connection Issue");
            res.send(sentAudio);
        }
    });


    app.get('/api/getAudioSentiments', ensureAuthorization, function (req, res) {
        var sent = {};
        sent.status = false;
        var conv_id = req.body.username || req.param('conv_id');
        log(conv_id);
        //First st Ten Charecters are Conv_id
        //http://localhost:3000/api/getAudioSentiments?conv_id=2044197581

        if (user.isDB) {
            var options = {};
            options.values = [];
            options.values.push(conv_id);
            options.timeout = 4000;
            options.sql = "select polpospercent, polnegpercent, Anger, Surprise, Sadness, Joy, Disgust, Fear, Trust, Anticipation from voice_sentiment where conv_id = ?";


            connection.query(options, function (err, rows, fields) {
                log(rows);
                if (!err) {
                    log(options.sql);
                    if (rows.length != 0) {
                        sent.status = true;
                        sent.data = rows;
                        log(rows.length);
                    }
                    res.send(sent);
                }

            });


        }
        else {
            log("DataBase Connection Issue");
            res.send(sent);
        }
    });


    app.get('/api/getDayWiseSentiment', ensureAuthorization, function (req, res) {
        var sent = {};
        sent.status = false;


        if (user.isDB) {
            var options = {};
            options.timeout = 4000;
            options.sql = "select dayname(file_ts) as dayname, avg(Anger) as Anger,avg(Surprise) as Surprise,avg(Sadness) as Sadness,avg(Joy) as Joy,avg(Disgust) as Disgust, avg(Fear) as Fear,avg(Trust) as Trust,avg(Anticipation) as Anticipation from sentimental_data group by dayname(file_ts) order by dayname";


            connection.query(options, function (err, rows, fields) {
                if (!err) {
                    log(options.sql);
                    if (rows.length != 0) {
                        sent.status = true;
                        sent.data = rows;
                        log(rows.length);
                    }
                    res.send(sent);
                }

            });


        }
        else {
            log("DataBase Connection Issue");
            res.send(sent);
        }
    });


})();
