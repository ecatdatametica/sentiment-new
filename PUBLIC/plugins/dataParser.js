(function($)
{


$.sortBy  =  function(jsonArray, key){
if(jsonArray){
var sortedArray = jsonArray.sort(function(left, right) { 
                 //array.sort is buit-in function
   var a = left[key];
   var b = right[key];
   if (a !== b) {
       if (a > b || a === void 0) return 1;
       if (a < b || b === void 0) return -1;
   }
   return 0;
});
return sortedArray;
}
},


$.getByField = function(res)
{
	
	var data = res.data;
	return data;
	/*var callData ={};
	callData.modelled_topic =[];
	callData.primary_topic =[];
	callData.secondary_topic =[];
	log(JSON.stringify(data));

	
	var modelledTopic= $.unique(data.map(function (d) 
	{
    topic ={};
    topic.primary_topic  = d.primary_topic;
    topic.secondary_topic = d.secondary_topic;
    topic.modelled_topic = 	d.modelled_topic;
    topic.count = d.count;
    return topic;

	}));*/
	
   /* var primary_topic= $.unique(data.map(function (d) {
    return d.primary_topic;}));

    var secondary_topic= $.unique(data.map(function (d) {
    return d.secondary_topic;}));

    callData.modelled_topic = modelled_topic;
    callData.primary_topic = primary_topic;
    callData.secondary_topic = secondary_topic;
    log(callData.modelled_topic[0]);
    var filt = $.filter(data);
    log(filt);
    return filt;
	*/

	
}

$.filterModellingData = function(res) 
{    
        var groups = {};
		res.forEach(function(val)
		{
		  if(!groups[val.primary_topic])
		  {
		  	    groups[val.primary_topic] = {};
		  		groups[val.primary_topic]['name'] = val.primary_topic;
		  		groups[val.primary_topic]['count'] = 0;
		  }
		  if(!groups[val.primary_topic][val.secondary_topic])
		  	{ 
		  		groups[val.primary_topic][val.secondary_topic] = {};
		  		groups[val.primary_topic][val.secondary_topic]['name'] = val.secondary_topic;
		  		groups[val.primary_topic][val.secondary_topic]['count'] = 0;
			}
			log(val.count);
			if (!groups[val.primary_topic][val.secondary_topic][val.modelled_topic])
			{
				groups[val.primary_topic][val.secondary_topic][val.modelled_topic] = {};
			}
		  //groups[val.secondary_topic]['name'] = val.secondary_topic;
		  groups[val.primary_topic][val.secondary_topic]['count'] += val.count;
		  groups[val.primary_topic]['count'] += val.count;
		  groups[val.primary_topic][val.secondary_topic][val.modelled_topic]['name']  = val.modelled_topic;
		  groups[val.primary_topic][val.secondary_topic][val.modelled_topic]['count']  = val.count;
		 // groups[val.primary_topic][val.secondary_topic] [val.modelled_topic] = groups["all"][val.modelled_topic] = 1;
		});




		return groups;
};



$.getDataByField = function(res,key)
{


var sum = 0;
var i=0;
var max =0;
var min = 100;
var ret ={};
var fieldOrder = [];
fieldOrder.push(key);
while(i!=res.length)
{
fieldOrder.push(res[i][key]);
var val = res[i][key];
sum = sum+val;
max = Math.max(val, max );
min = Math.min(val, min);
i+=1;
}

ret.name= key;
ret.sum = sum;
ret.percent = (sum/res.length);
ret.max = max;
ret.min = min;
ret.arrayList = fieldOrder;

return ret;

}

$.formatGaugeData = function(res)
{

var gaugeData = {};
gaugeData.columns = [];
var gaugeArray =[];
gaugeArray.push(res.name);
gaugeArray.push(res.percent);
gaugeData.columns.push(gaugeArray);
gaugeData.type="gauge";
return gaugeData; 		
}

$.formatDonutData =  function(res)
{
var i=0;
var donutData = {};
donutData.columns = [];
while(i!=res.length)
{
var donutArray =[];
donutArray.push(res[i].name);
donutArray.push(res[i].sum);
donutData.columns.push(donutArray);
i+=1;
}
donutData.type="donut";
return donutData;
}

$.formatChartTimeSeries = function(res,resDate)
{


var data = {};
data.columns = [];
data.selection= {};
data.columns.push(resDate.dateList);
data.selection.multiple =true;
data.selection.draggable =true;

data.x = resDate.name;
var i=0;
while(i!=res.columns.length)
{
data.columns.push(res.columns[i].arrayList);
i+=1;
}
return data;
}

$.formatStackedChart = function(res)
{
var data = {};
data.columns = [];
data.groups =[];
group =[];

var i=0;
while(i!=res.length)
{
data.columns.push(res[i].arrayList);
group.push(res[i].name)
i+=1;
}
//  data.groups.push(group);
data.type="bar";



return data;

}



$.getDates = function(sort)
{
var date ={};
date.name = "Dates";
date.dateList = [];
var i=0;
date.dateList.push("Dates");
while(i!=sort.length)
{
date.dateList.push(new Date(sort[i].file_ts));
i+=1;
}

date.startDate =sort[0].file_ts;
date.endDate =sort[sort.length-1].file_ts;
/*log(new Date(sort[sort.length-1].file_tsNew) + " "+sort[sort.length-1].file_tsNew);
date.startDate = new Date(sort[0].file_tsNew);
date.endDate = new Date(sort[sort.length-1].file_tsNew);*/

return date;


}

$.getDaysAgo =  function(b)
{
try
{

var a=new Date;
a.setDate(a.getDate()-b);
var b = a.toString();

var split = b.split(" ");
var splitDate = split[2].split("");

//  var dateComp = split[2]+"-"+split[1]+"-"+split[3];
if(parseInt(splitDate[0])==0)
{
var dateComp = splitDate[1]+"-"+split[1]+"-"+split[3];
}
else
{
var dateComp = split[2]+"-"+split[1]+"-"+split[3] 
}


return dateComp; 

}   
catch(e)
{
console.log(e);
}

}


$.formatDate = function(date)
{
var d = new Date(date),
month = '' + (d.getMonth() + 1),
day = '' + d.getDate(),
year = d.getFullYear();
if (month.length < 2) month = '0' + month;
if (day.length < 2) day = '0' + day;

return [year, month, day].join('-');


}


$.formatForStackedCharts = function(res)
{
	

	var Anger =[];
	var Anticipation = [];
	var Disgust = [];
	var Fear = [];
	var Joy = [];
	var Sadness = [];
	var Surprise = [];
	var Trust = [];



	Anger.push("Anger");
	Anticipation.push("Anticipation");
	Disgust.push("Disgust");
	Fear.push("Fear");
	Joy.push("Joy");
	Sadness.push("Sadness");
	Surprise.push("Surprise");
	Trust.push("Trust");



	var data = {};
	data.columns = [];
	data.groups =[];
	group =[];
	


	var i=0;
	while(i!=res.data.length)
	{
		Anger.push(res.data[i].Anger);
		Anticipation.push(res.data[i].Anticipation);
		Disgust.push(res.data[i].Disgust);
		Fear.push(res.data[i].Fear);
		Joy.push(res.data[i].Joy);
		Sadness.push(res.data[i].Sadness);
		Surprise.push(res.data[i].Surprise);
		Trust.push(res.data[i].Trust);

		if(i==0)
		{
			group.push(res.data[i].Anger);
			group.push(res.data[i].Anticipation);
			group.push(res.data[i].Disgust);
			group.push(res.data[i].Fear);
			group.push(res.data[i].Joy);
			group.push(res.data[i].Sadness);
			group.push(res.data[i].Surprise);
			group.push(res.data[i].Trust);


		}


	i+=1;
	}

	data.columns.push(Anger);
	data.columns.push(Anticipation);
	data.columns.push(Disgust);
	data.columns.push(Fear);
	data.columns.push(Joy);
	data.columns.push(Sadness);
	data.columns.push(Surprise);
	data.columns.push(Trust);
	//  data.groups.push(group);
	data.type="bar";
	data.groups.push(group);

	
	return data;



}




$.formatForTimeSeries = function(res)
{

	var Anger =[];
	var Anticipation = [];
	var Disgust = [];
	var Fear = [];
	var Joy = [];
	var Sadness = [];
	var Surprise = [];
	var Trust = [];
	var file_ts = [];
	var record_count = [];

	file_ts.push("file_ts");
	Anger.push("Anger");
	Anticipation.push("Anticipation");
	Disgust.push("Disgust");
	Fear.push("Fear");
	Joy.push("Joy");
	Sadness.push("Sadness");
	Surprise.push("Surprise");
	Trust.push("Trust");
	record_count.push("record_count");


	var data = {};
	data.columns = [];
	data.types ={};
	data.axes = {};


	var i=0;
	while(i!=res.data.length)
	{
		// 		file_ts.push(new Date(res.data[i].file_ts));
		file_ts.push(new Date(res.data[i].file_time));
		Anger.push(res.data[i].Anger);
		Anticipation.push(res.data[i].Anticipation);
		Disgust.push(res.data[i].Disgust);
		Fear.push(res.data[i].Fear);
		Joy.push(res.data[i].Joy);
		Sadness.push(res.data[i].Sadness);
		Surprise.push(res.data[i].Surprise);
		Trust.push(res.data[i].Trust);
		record_count.push(res.data[i].record_count);

	i+=1;
	}

	data.columns.push(Anger);
	data.columns.push(Anticipation);
	data.columns.push(Disgust);
	data.columns.push(Fear);
	data.columns.push(Joy);
	data.columns.push(Sadness);
	data.columns.push(Surprise);
	data.columns.push(Trust);
	data.columns.push(file_ts);
	data.columns.push(record_count);


	data.colors ={};
	data.colors.Anger= '#FF6600';
    data.colors.Anticipation= '#2A16BB';
    data.colors.Disgust= '#FF00FF';
    data.colors.Fear= '#39BB16';
    data.colors.Joy= '#9933FF';
    data.colors.Sadness= '#D6941E';
    data.colors.Surprise= '#BB1669';
    data.colors.Trust= '#3399FF';
    data.colors.record_count= '#198D96';
	//  data.groups.push(group);
	data.x = 'file_ts';
	data.axes.record_count = 'y2';
	//data.type ="bar";  //comment this line if want to change to timeseries graph
	data.types.record_count ="bar";
/*	data.types.Anger ="bar";
	data.types.Joy ="bar";*/
	//log(JSON.stringify(data));
	log(data);
	return data;
}





$.formatForSplineChart = function(res)
{

	var Anger =[];
	var Anticipation = [];
	var Disgust = [];
	var Fear = [];
	var Joy = [];
	var Sadness = [];
	var Surprise = [];
	var Trust = [];



	Anger.push("Anger");
	Anticipation.push("Anticipation");
	Disgust.push("Disgust");
	Fear.push("Fear");
	Joy.push("Joy");
	Sadness.push("Sadness");
	Surprise.push("Surprise");
	Trust.push("Trust");



	var data = {};
	data.columns = [];
	data.groups =[];
	data.types ={};
	group =[];
	


	var i=0;
	while(i!=res.data.length)
	{
		Anger.push(res.data[i].Anger);
		Anticipation.push(res.data[i].Anticipation);
		Disgust.push(res.data[i].Disgust);
		Fear.push(res.data[i].Fear);
		Joy.push(res.data[i].Joy);
		Sadness.push(res.data[i].Sadness);
		Surprise.push(res.data[i].Surprise);
		Trust.push(res.data[i].Trust);

		if(i==0)
		{
			group.push(res.data[i].Anger);
			group.push(res.data[i].Anticipation);
			group.push(res.data[i].Disgust);
			group.push(res.data[i].Fear);
			group.push(res.data[i].Joy);
			group.push(res.data[i].Sadness);
			group.push(res.data[i].Surprise);
			group.push(res.data[i].Trust);


		}


	i+=1;
	}

	data.types.Anger ="area-spline";
	data.types.Anticipation ="area-spline";
	data.types.Disgust ="area-spline";
	data.types.Fear ="area-spline";
	data.types.Joy ="area-spline";
	data.types.Sadness ="area-spline";
	data.types.Surprise ="area-spline";
	data.types.Trust ="area-spline";

	data.columns.push(Anger);
	data.columns.push(Anticipation);
	data.columns.push(Disgust);
	data.columns.push(Fear);
	data.columns.push(Joy);
	data.columns.push(Sadness);
	data.columns.push(Surprise);
	data.columns.push(Trust);

	data.colors ={};
	data.colors.Anger= '#FF6600';
    data.colors.Anticipation= '#2A16BB';
    data.colors.Disgust= '#FF00FF';
    data.colors.Fear= '#39BB16';
    data.colors.Joy= '#9933FF';
    data.colors.Sadness= '#D6941E';
    data.colors.Surprise= '#BB1669';
    data.colors.Trust= '#3399FF';

	//  data.groups.push(group);
	data.type="bar";
	data.groups.push(group);

	
	return data;



}

$.formatForPieCharts = function(res)
{
	
	var data = {};
	data.columns = [];
	


	var Anger =[];
	var Anticipation = [];
	var Disgust = [];
	var Fear = [];
	var Joy = [];
	var Sadness = [];
	var Surprise = [];
	var Trust = [];



	Anger.push("Anger");
	Anticipation.push("Anticipation");
	Disgust.push("Disgust");
	Fear.push("Fear");
	Joy.push("Joy");
	Sadness.push("Sadness");
	Surprise.push("Surprise");
	Trust.push("Trust");

	var AngerSum=0;
	var AnticipationSum=0;
	var DisgustSum=0;
	var FearSum=0;
	var JoySum=0;
	var SadnessSum=0;
	var SurpriseSum=0;
	var TrustSum=0;

	var i=0;
	while(i!=res.data.length)
	{
		AngerSum += res.data[i].Anger;
		AnticipationSum += res.data[i].Anticipation;
		DisgustSum += res.data[i].Disgust;
		FearSum += res.data[i].Fear;
		JoySum += res.data[i].Joy;
		SadnessSum +=  res.data[i].Sadness;
		SurpriseSum += res.data[i].Surprise;
		TrustSum += res.data[i].Trust;
	i+=1;
	}


	Anger.push(AngerSum);
	Anticipation.push(AnticipationSum);
	Disgust.push(DisgustSum);
	Fear.push(FearSum);
	Joy.push(JoySum);
	Sadness.push(SadnessSum);
	Surprise.push(SurpriseSum);
	Trust.push(TrustSum);

	data.columns.push(Anger);
	data.columns.push(Anticipation);
	data.columns.push(Disgust);
	data.columns.push(Fear);
	data.columns.push(Joy);
	data.columns.push(Sadness);
	data.columns.push(Surprise);
	data.columns.push(Trust);

	data.colors ={};
	data.colors.Anger= '#FF6600';
    data.colors.Anticipation= '#2A16BB';
    data.colors.Disgust= '#FF00FF';
    data.colors.Fear= '#39BB16';
    data.colors.Joy= '#9933FF';
    data.colors.Sadness= '#D6941E';
    data.colors.Surprise= '#BB1669';
    data.colors.Trust= '#3399FF';

	data.type  = 'pie';
	return data;

}


$.formatForDayWiseSent = function(type,title,res)
{
	
	var data = {};
	data.columns = [];
	log(res);
	log(res.Anger);

	var Anger =[];
	var Anticipation = [];
	var Disgust = [];
	var Fear = [];
	var Joy = [];
	var Sadness = [];
	var Surprise = [];
	var Trust = [];



	Anger.push("Anger");
	Anticipation.push("Anticipation");
	Disgust.push("Disgust");
	Fear.push("Fear");
	Joy.push("Joy");
	Sadness.push("Sadness");
	Surprise.push("Surprise");
	Trust.push("Trust");

	var AngerSum=0;
	var AnticipationSum=0;
	var DisgustSum=0;
	var FearSum=0;
	var JoySum=0;
	var SadnessSum=0;
	var SurpriseSum=0;
	var TrustSum=0;

		AngerSum = res.Anger;
		AnticipationSum = res.Anticipation;
		DisgustSum = res.Disgust;
		FearSum = res.Fear;
		JoySum = res.Joy;
		SadnessSum =  res.Sadness;
		SurpriseSum = res.Surprise;
		TrustSum = res.Trust;
	


	Anger.push(AngerSum);
	Anticipation.push(AnticipationSum);
	Disgust.push(DisgustSum);
	Fear.push(FearSum);
	Joy.push(JoySum);
	Sadness.push(SadnessSum);
	Surprise.push(SurpriseSum);
	Trust.push(TrustSum);

	data.columns.push(Anger);
	data.columns.push(Anticipation);
	data.columns.push(Disgust);
	data.columns.push(Fear);
	data.columns.push(Joy);
	data.columns.push(Sadness);
	data.columns.push(Surprise);
	data.columns.push(Trust);
	
	data.colors ={};
	data.colors.Anger= '#FF6600';
    data.colors.Anticipation= '#2A16BB';
    data.colors.Disgust= '#FF00FF';
    data.colors.Fear= '#39BB16';
    data.colors.Joy= '#9933FF';
    data.colors.Sadness= '#D6941E';
    data.colors.Surprise= '#BB1669';
    data.colors.Trust= '#3399FF';
    data.title = title;
	data.type  = type;
	return data;

}









$.formatForDonutCharts = function(res)
{

	var data = {};
	data.columns = [];
	


	var Polpositive =[];
	var Polnegative = [];
	


	Polpositive.push("Positve");
	Polnegative.push("Negative");
	

	var PolpositiveSum=0;
	var PolnegativeSum=0;


	var i=0;
	while(i!=res.data.length)
	{
		PolpositiveSum += res.data[i].polpospercent;
		PolnegativeSum += res.data[i].polnegpercent;	
		i+=1;
	}


	Polpositive.push(PolpositiveSum);
	Polnegative.push(PolnegativeSum);

	data.colors ={};
	
	

	data.columns.push(Polpositive);
	data.columns.push(Polnegative);
	
	data.type  = 'donut';
	data.title ="Overall Polarity";
	
	return data;

}

$.getAllDepartment = function(res)
{
	//Get List Of All Department
	var i=0;
	var Dept = [];
	while(res.data.length!=0)
	{

		i+=1;
	}



}


$.getUniqueDept = function(res)
{
	var data = res.data;
	var UniqueNames= $.unique(data.map(function (d) {
    return d.dept_id;}));

	return UniqueNames;
}


$.getUniqueRep = function(res)
{
	var data = res.data;
	var UniqueNames= $.unique(data.map(function (d) {
    return d.rep_id;}));

	return UniqueNames;
}
$.getUniqueCategory = function(res)
{
	var data = res.data;
	var UniqueNames= $.unique(data.map(function (d) {
    return d.category_id;}));

	return UniqueNames;
}

$.getUniqueChannel = function(res)
{
	var data = res.data;
	var UniqueNames= $.unique(data.map(function (d) {
    return d.channel;}));

	return UniqueNames;
}

$.performFilter = function(res)
{
	


}


$.getUniqueDay = function(res)
{
	var data;

	if(res.data)
	{
		data =res.data;
	}
	else
	{
	    data =res;
	}
	
	var uniQueDays= $.unique(data.map(function (d) {
    return d.Day;}));
	return uniQueDays;
}


$.formatForDayWise = function(res,key)
{
	log(res);
	var i = 0;
	var obj =[];
	while(i!=res.data.length)
	{
		if(key==res.data[i].sentiments)
		{
			obj = res.data[i];
			break;
		}
		i+=1;
	}
	log(obj);
	log("Formatting Value For Pie Chart");
	var data = {};
	data.columns = [];
	
	var Monday =[];
	var Tuesday = [];
	var Wednesday = [];
	var Thursday = [];
	var Friday = [];
	var Saturday = [];
	var Sunday = [];



	Monday.push("Monday");
	Tuesday.push("Tuesday");
	Wednesday.push("Wednesday");
	Thursday.push("Thursday");
	Friday.push("Friday");
	Saturday.push("Saturday");
	Sunday.push("Sunday");

	Monday.push(obj.Monday);
	Tuesday.push(obj.Tuesday);
	Wednesday.push(obj.Wednesday);
	Thursday.push(obj.Thursday);
	Friday.push(obj.Friday);
	Saturday.push(obj.Saturday);
	Sunday.push(obj.Sunday);

	data.columns.push(Monday);
	data.columns.push(Tuesday);
	data.columns.push(Wednesday);
	data.columns.push(Thursday);
	data.columns.push(Friday);
	data.columns.push(Saturday);
	data.columns.push(Sunday);
	data.title = key;
	data.type  = 'donut';

	data.colors ={};
	data.colors.Monday= '#F51919';
    data.colors.Tuesday= '#D6941E';
    data.colors.Wednesday= '#9ABB16';
    data.colors.Thursday= '#39BB16';
    data.colors.Friday= '#16BB8C';
    data.colors.Saturday= '#2A16BB';
    data.colors.Sunday= '#BB1669';
        

	log(data);
	return data;
}

$.attachTextToPie = function(indx){
	var data = [];

	var allTexts = {
			id0 : [{name: "Anger", desc: ["problem"]},
				{name: "Surprise", desc: ["shopping"]},
				{name: "Sadness", desc: ['problem', 'worry']},
				{name: "Joy", desc: ['shopping', 'share', 'thank']},
				{name: "Disgust", desc: [""]},
				{name: "Fear", desc: ['problem', 'worry', 'government']},
				{name: "Trust", desc: ['personal', 'shopping', 'share']},
				{name: "Anticipation", desc: ['shopping', 'notification', 'share', 'store', 'mail', 'bye', 'worry']}
			],
			id1 : [{name: "Anger", desc: [""]},
				{name: "Surprise", desc: ["leave", "good"]},
				{name: "Sadness", desc: ["leave"]},
				{name: "Joy", desc: ['good', 'daughter', 'glad', 'happy']},
				{name: "Disgust", desc: [""]},
				{name: "Fear", desc: [""]},
				{name: "Trust", desc: ['account', 'good', 'happy']},
				{name: "Anticipation", desc: ['good', 'glad', 'coming', 'bye', 'store', 'happy']}
			],
				id2 : [{name: "Anger", desc: ["cash"]},
				{name: "Surprise", desc: [""]},
				{name: "Sadness", desc: [""]},
				{name: "Joy", desc: ['thank',' welcome', 'cash']},
				{name: "Disgust", desc: [""]},
				{name: "Fear", desc: ['cash']},
				{name: "Trust", desc: ['assist', 'cash']},
				{name: "Anticipation", desc: ['cash', 'wait']}
			],
		id3 : [{name: "Anger", desc: [""]},
			{name: "Surprise", desc: ["good"]},
			{name: "Sadness", desc: ['disconnect']},
			{name: "Joy", desc: ['thank', 'good']},
			{name: "Disgust", desc: [""]},
			{name: "Fear", desc: [""]},
			{name: "Trust", desc: ["good"]},
			{name: "Anticipation", desc: ["good"]}
		],
		id4 : [{name: "Anger", desc: [""]},
			{name: "Surprise", desc: [""]},
			{name: "Sadness", desc: ['problem']},
			{name: "Joy", desc: ['thank', 'comfort']},
			{name: "Disgust", desc: [""]},
			{name: "Fear", desc: ['problem']},
			{name: "Trust", desc: ['thank', 'comfort']},
			{name: "Anticipation", desc: ['comfort']}
		],
		id5 : [{name: "Anger", desc: [""]},
			{name: "Surprise", desc: ['wonderful']},
			{name: "Sadness", desc: [""]},
			{name: "Joy", desc: ['perfect', 'wonderful', 'glad', 'thank']},
			{name: "Disgust", desc: [""]},
			{name: "Fear", desc: ['change']},
			{name: "Trust", desc: ['perfect', 'wonderful']},
			{name: "Anticipation", desc: ['perfect', 'start', 'glad', 'time', 'mail', 'store']}
		],

		id6 : [{name: "Anger", desc: [""]},
			{name: "Surprise", desc: [""]},
			{name: "Sadness", desc: [""]},
			{name: "Joy", desc: ['thank',' welcome','perfect']},
			{name: "Disgust", desc: ['boy']},
			{name: "Fear", desc: ['change']},
			{name: "Trust", desc: ['perfect', 'specialist', 'account', 'transaction', 'verification']},
			{name: "Anticipation", desc: ['perfect', 'start', 'mail']}
		]
	};

	var regex = /(\d+)/g;
	//alert(text.match(regex));


	for(var i in allTexts){
		if(indx == i.match(regex)){
			data = allTexts[i]
		}
	}
	return data;

}


$.attachTextToDonut = function(indx){
		var data = [];

		var allTexts = {
			id0 : [{name: "Positve", desc: ['information', 'shopping', 'option', 'ahead', 'share', 'store']},
				   {name: "Negative", desc: ['problem', 'worry']}
			],
			id1 : [{name: "Positve", desc: ['information', 'good', 'daughter', 'question', 'contact', 'store', 'glad', 'happy']},
				{name: "Negative", desc: ["leave"]}
			],
			id2 : [{name: "Positve", desc: ['assist', 'primary', 'working', 'cash']},
				{name: "Negative", desc: ["wait"]}
			],
			id3 : [{name: "Positve", desc: ['thank', 'good']},
				{name: "Negative", desc: ['disconnect']}
			],
			id4 : [{name: "Positve", desc: ['assist', 'comfort', 'ahead']},
				{name: "Negative", desc: ['problem']}
			],
			id5 : [{name: "Positve", desc: ['perfect', 'wonderful', 'store', 'glad', 'thank']},
				{name: "Negative", desc: [""]}
			],
			id6 : [{name: "Positve", desc: ['perfect', 'oak', 'authorized', 'ahead', 'verification']},
				{name: "Negative", desc: [""]}
			]
		};

		var regex = /(\d+)/g;
		//alert(text.match(regex));


		for(var i in allTexts){
			if(indx == i.match(regex)){
				data = allTexts[i]
			}
		}
		return data;

}


})(jQuery)



/*

$.getUniqueDept = function(res)
{
	var data = res.data;
	var UniqueNames= $.unique(data.map(function (d) {
    return d.dept_id;}));

	return UniqueNames;
}


$.getUniqueRep = function(res)
{
	var data = res.data;
	var UniqueNames= $.unique(data.map(function (d) {
    return d.rep_id;}));

	return UniqueNames;
}
$.getUniqueCategory = function(res)
{
	var data = res.data;
	var UniqueNames= $.unique(data.map(function (d) {
    return d.category_id;}));

	return UniqueNames;
}

$.getUniqueChannel = function(res)
{
	var data = res.data;
	var UniqueNames= $.unique(data.map(function (d) {
    return d.channel;}));

	return UniqueNames;
}

$.performFilter = function(res)
{
	


}*/