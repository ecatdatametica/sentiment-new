(function($)
{

$.drawSpline =  function(ele,data)
{
c3.generate({
bindto: '#'+ele,
data: data
});


}



$.drawStackedBar  = function(ele,data)
{


c3.generate({
bindto: '#'+ele,
data: data,
bar: 
{
width: 
{
ratio: 1.5 // this makes bar width 50% of length between ticks
}
},
grid: 
{
y: 
{
lines: [{value:0}]
}
},
subchart: 
{
show: true
},
tooltip: 
{
grouped: true // Default true
},
zoom: 
{
enabled: true
},
grid: 
{
x: {
show: true
},
y: {
show: true
}
}
});  /*Draw Function Ends Here*/





};

$.drawTimeSeries = function(ele,data,drawType)
{
if(drawType=="weekly")
{
dateForm = d3.time.format("%a-%c");
}
if(drawType=="yesterday")
{
dateForm = d3.time.format("%H-%c");
}
if(drawType=="monthly")
{
dateForm = d3.time.format("%b");
}
if(drawType=="yearly")
{
dateForm = d3.time.format("%B-%d");

}



log("Drawing Data For TimeSeries");
log(data);
var chart = null;
c3.generate({
bindto: '#'+ele,
data: data,
tooltip: 
{
format: 
{
title: function (d) { return '' + new Date(d); },
value: function (value, ratio, id) 
{
	var val;
//	log(value,ratio,id);
    if(id=="record_count")
    {
    	var format = d3.format('');
    	val = format(value);
    }
    else
    {	
    var format = d3.format('%');
     val = format(value/100);
    }
    return val;
}  //func ends
}  //format ends
},  //tooltip ends
grid: 
{
x: 
{
show: false
},
y: 
{
show: false
}
},
zoom: 
{
enabled: true,
rescale: true
},
subchart: 
{
show: true
},
interaction: 
{
enabled: true
},
	axis: 
	{
		y : 
		{
			max:100,
			min:0
		},
		x: 
		{
			type: 'timeseries',
		},
		y2: 
		{
			show: true
		}
	}  //Axis Ends
});
} /*Func ENds*/

$.drawPie = function(ele,data)
        {

                c3.generate({
                bindto: '#'+ele,
                data: data,
                axis: {
                y:  {
                    label:
                    { // ADD
                    text: 'Y Label',
                    position: 'outer-middle'
                    }
                },
                tick:
                {
                format: d3.format("$,") // ADD
                },

                y2:
                    {
                    show: true,
                    label: { // ADD
                    text: 'Y2 Label',
                    position: 'outer-middle'
                    }
                }
        }
});

}  /*Func ENds*/

    $.drawPiewithWords = function(ele,data,words)
    {

        console.log(JSON.stringify(data));

        c3.generate({
            bindto: '#'+ele,
            data: data,

            tooltip: {
                format: {

                },
                grouped: false,

                contents: function (data, defaultTitleFormat, defaultValueFormat, color) {
                    var $$ = this, config = $$.config,
                        titleFormat = config.tooltip_format_title || defaultTitleFormat,
                        nameFormat = config.tooltip_format_name || function (name) { return name; },
                        valueFormat = config.tooltip_format_value || defaultValueFormat,
                        text, i, title, value, word;

                    //var words = ["aaa", "bbb", "ccc", "ddd", "fff", "lll"];

                    for (i = 0; i < data.length; i++) {
                        if (! (data[i] && (data[i].value || data[i].value === 0))) { continue; }

                        if (! text) {
                            title = titleFormat ? titleFormat(data[i].name) : data[i].name;
                            text = "<div id='tooltip' class='d3-tip'>";
                        }
                        value = valueFormat(data[i].value, data[i].ratio, data[i].id, data[i].index);

                        for(var z=0; z<words.length; z++){

                            console.log(data[i].name);

                            if(data[i].name == words[z].name){
                                var word = words[z].desc;
                            };
                        }


                        text += "<span class='info'>"+ title +"</span><br>";
                        text += "<span class='value'>" + value + "</span><br>";
                        text += "<span class='info'>Words:" + word + "</span>";
                        text += "</div>";
                    }

                    return text;
                }
            }
        });

    }  /*Func ENds*/


$.drawGauge = function(ele,data)
{

c3.generate({
bindto: '#'+ele,
data: data,
gauge: {
// label: {
//      format: function(value, ratio) {
//          return value;
//      },
//       show: false // to turn off the min/max labels.
//     },
// min: 0, // 0 is default, //can handle negative min e.g. vacuum / voltage / current flow / rate of change
// /    max: 100, // 100 is default
//    units: ' %',
// width: 39 // for adjusting arc thickness
},
color: 
{
pattern: ['#FF0000', '#22E41D','#F97600','#F6C600', '#60B044','#13B7FF','#00FF0D'], // the three color levels for the percentage values.
threshold: 
{
//            	unit: 'value', // percentage is default
//            	max: 200, // 100 is default
values: [5,10,15,20,25,30,40]
}
}
});
}


$.drawDonut = function(ele,data)
{
log("Drawing Donut");	
log(data);
c3.generate({
bindto: '#'+ele,
data: data,
donut: 
{
title:data.title
}
});
}

$.drawdonutwithWords = function(ele,data,words)
{

    c3.generate({
        bindto: '#'+ele,
        data: data,
        donut:
        {
            title:data.title
        },

        tooltip: {
            format: {

            },
            grouped: false,

            contents: function (data, defaultTitleFormat, defaultValueFormat, color) {
                var $$ = this, config = $$.config,
                    titleFormat = config.tooltip_format_title || defaultTitleFormat,
                    nameFormat = config.tooltip_format_name || function (name) { return name; },
                    valueFormat = config.tooltip_format_value || defaultValueFormat,
                    text, i, title, value, word;

                //var words = ["aaa", "bbb", "ccc", "ddd", "fff", "lll"];

                for (i = 0; i < data.length; i++) {
                    if (! (data[i] && (data[i].value || data[i].value === 0))) { continue; }

                    if (! text) {
                        title = titleFormat ? titleFormat(data[i].name) : data[i].name;
                        text = "<div id='tooltip' class='d3-tip'>";
                    }
                    value = valueFormat(data[i].value, data[i].ratio, data[i].id, data[i].index);

                    for(var z=0; z<words.length; z++){

                        console.log(data[i].name);

                        if(data[i].name == words[z].name){
                            var word = words[z].desc;
                        };
                    }


                    text += "<span class='info'>"+ title +"</span><br>";
                    text += "<span class='value'>" + value + "</span><br>";
                    text += "<span class='info'>Words:" + word + "</span>";
                    text += "</div>";
                }

                return text;
            }
        }
    });

}  /*Func ENds*/






})(jQuery)












