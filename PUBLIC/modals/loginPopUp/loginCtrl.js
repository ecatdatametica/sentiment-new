app.controller('loginCtrl', function ($location,$http,Auth,$rootScope,$scope) {
	
	$rootScope.loginMessage = "Login";
	$rootScope.hideTopMenu = false;
	$rootScope.hideLoader=true;
	$scope.authModal = function(user,pass)
	{
		
		if(user!=undefined && pass!=undefined)
		{
			if(user.length!=0 && pass.length!=0)
			{	

			$scope.alertShow=true;
			$scope.alertType = "alert alert-warning";
			$scope.alertMessage ="Logging In...Please Wait";
			var credentials = {};
			credentials.username =user;
			credentials.password = pass;
			Auth.init(credentials).then(function(res)
			{
				log(res);
				if(res.data.authenticated)
				{	
				$rootScope.isUserAuthenticated =true;
				$scope.alertType = "alert alert-success";
				$scope.alertMessage ="......Success";
				$rootScope.isUserAuth = res.data;
				$rootScope.token =res.data.token;
				log($rootScope.token);
				$http.defaults.headers.common.Authorization = $rootScope.token;
			
				

				$rootScope.hideTopMenu = true;
				$rootScope.isUserAuth = true;
				$location.path("/landing");
				
				}
				else
				{
					$rootScope.isUserAuthenticated =false;
					$scope.alertType = "alert alert-danger";
					$scope.alertMessage ="Username Not Found";
					$location.path("/");
				}	
						

			});

			}
			else
			{
				$rootScope.alertShow=true;
				$scope.alertType = "alert alert-danger";
				$scope.alertMessage ="Credential Empty";

			}

		}
		else
		{
			$rootScope.alertShow=true;
			$scope.alertType = "alert alert-danger";
			$scope.alertMessage ="Credential Empty";

		}
	}
});
