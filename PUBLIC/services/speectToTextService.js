/*/api/getAudioSentiments*/


var speech = angular.module("SpeechSentiment",["ngRoute"]);

speech.service('Speech', function ($http,$q,$rootScope) 
{
    var speechSentimentsRes = [];
    var speechSentimentAPI = Prod.host+ "api/getAudioSentiments";
   	return {
   		getSpeechSentiments : function(file)
	    {

	    	var TemConvId = file.substring(0,10);
	    	var conv = {};
	    		conv.conv_id = TemConvId;
	 		 speechSentimentsRes  = $http.get(speechSentimentAPI,
	 		 	{params :conv});
	 		 log(speechSentimentsRes);
	         return speechSentimentsRes;
	    }
   	}
});  

