var topic = angular.module('TopicSentiment',["ngRoute","sentAnalysis"]);



topic.service('Topic',function ($q,$http,Sentiments,$rootScope) 
{
	var api =  Prod.host+"api/sentiment";
	var yesterday=[];
	var weekly=[];
	var monthly=[];
	var yearly=[];

	return {
		yesterday : function(params)
		{
			log("Yesterday Data");
			if(yesterday.length!=0)
			{
				log("Sending Stored Data For Yesterday");
				return yesterday;
			}
			else
			{
				/*For Yesterday*/
		      var d1 = Sentiments.getDaysAgo(1);
		      var startDate =  Sentiments.formatDate(d1);
		      var d2 = Sentiments.getDaysAgo(0);
		      var endDate =  Sentiments.formatDate(d2);
		      var params = {};
		      params.startDate = startDate;
		      params.endDate = endDate;
			  yesterday = $http.get(api,
                      {
                      params :params
                      }).success(function(res)
                      {
                      	log(res);
                      	
                      });

               $q.all([yesterday]).then(function(resData)
               {
               	log(resData[0].data);
               	return resData;
               });       

			}	
		}, /*Function Ends*/
		weekly : function(params)
		{
			log("Weekly Data");
			if(weekly.length!=0)
			{
				log("Sending Stored Data For Weekly");
				return weekly;
			}
			else
			{
				/*For Yesterday*/
		      var d1 = Sentiments.getDaysAgo(7);
		      var startDate =  Sentiments.formatDate(d1);
		      var d2 = Sentiments.getDaysAgo(0);
		      var endDate =  Sentiments.formatDate(d2);
		      var params = {};
		      params.startDate = startDate;
		      params.endDate = endDate;
			  weekly = $http.get(api,
                      {
                      params :params
                      });
			}	return weekly;
		},/*Function Ends*/
		monthly : function(params)
		{
			log("Monthly Data");
			if(monthly.length!=0)
			{
				log("Sending Stored Data For Monthly");
				return monthly;
			}
			else
			{
				/*For Yesterday*/
		      var d1 = Sentiments.getDaysAgo(30);
		      var startDate =  Sentiments.formatDate(d1);
		      var d2 = Sentiments.getDaysAgo(0);
		      var endDate =  Sentiments.formatDate(d2);
		      var params = {};
		      params.startDate = startDate;
		      params.endDate = endDate;
			  monthly = $http.get(api,
                      {
                      params :params
                      });
			}	return monthly;
		}, /*Function Ends*/
		yearly : function(params)
		{
			log("Yearly Data");
			if(yearly.length!=0)
			{
				log("Sending Stored Data For Yearly");
				return yearly;
			}
			else
			{
				/*For Yesterday*/
		      var d1 = Sentiments.getDaysAgo(30);
		      var startDate =  Sentiments.formatDate(d1);
		      var d2 = Sentiments.getDaysAgo(0);
		      var endDate =  Sentiments.formatDate(d2);
		      var params = {};
		      params.startDate = startDate;
		      params.endDate = endDate;
			  yearly = $http.get(api,
                      {
                      params :params
                      });
			}	return yearly;
		} /*Function Ends*/
	}	/*Return Ends*/
}); /*Services Ends*/