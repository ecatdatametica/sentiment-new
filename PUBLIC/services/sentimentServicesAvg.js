var aggregate = angular.module("aggSentiment",["ngRoute","sentAnalysis"]);



aggregate.service('aggSent',function ($http,Sentiments,$rootScope) 
{
		//REST API CALLS URL 
		var totalDataAPI =  Prod.host+"api/totalData";
		var totalChatAPI =  Prod.host+"api/totalDataChat";
		var totalEmailAPI =  Prod.host+"api/totalDataEmail";
		var totalVoiceAPI =  Prod.host+"api/totalDataVoice";
		var totalWeekDayWise =  Prod.host+"api/WeekDayWise";
		var getTotalMappedAPI =  Prod.host+"api/getCombinedFiles";
		var getAudioTextAPI =  Prod.host+"api/audioText";
		var getAudioTextSentAPI = Prod.host+"api/audioTextSentimentsSearch";

		var totalDataRes = [];
		var totalChatRes =[];
		var totalEmailRes =[];
		var totalVoiceRes = [];
		var totalWeekRes = [];
		var getTotalMappedRes=[];
		var getAudioTextSentRes=[];
			//Get Day Wise Data
		var totalDayWiseAPI =  Prod.host+"api/getDayWiseSentiment";
		var totalDayWiseRes = [];


		return {

		getAudioListBySearch : function(searchText)
		{
			search = {};
			search.searchText = searchText.toLowerCase();
            log(search)
			 getAudioTextSentRes = $http.get(getAudioTextSentAPI,
                 {
			 	params : search
			 });
			 return getAudioTextSentRes;


		},


		getTotalData : function() 
		{
			if(totalDataRes.length!=0)
			{
				log("getTotalData Already Called .Returning Stored");
				return totalDataRes;
			}
			else
			{
				totalDataRes = $http.get(totalDataAPI);
				return totalDataRes;
			}	
			
		}, //Function Ends
		getTotalChatData : function() 
		{
			if(totalChatRes.length!=0)
			{
				log("getTotalChatData Already Called .Returning Stored");
				return totalChatRes;
			}
			else
			{
				totalChatRes = $http.get(totalChatAPI);
				return totalChatRes;
			}	
			
		}, //Function Ends
		getTotalEmailData : function() 
		{
			if(totalEmailRes.length!=0)
			{
				log("totalEmailRes Already Called .Returning Stored");
				return totalEmailRes;
			}
			else
			{
				totalEmailRes = $http.get(totalEmailAPI);
				return totalEmailRes;
			}	
			
		}, //Function Ends
		getTotalVoiceData : function() 
		{
			if(totalVoiceRes.length!=0)
			{
				log("totalEmailRes Already Called .Returning Stored");
				return totalVoiceRes;
			}
			else
			{
				totalVoiceRes = $http.get(totalVoiceAPI);
				return totalVoiceRes;
			}	
			
		}, //Function End
		getTotalWeekDayWise : function() 
		{
			if(totalWeekRes.length!=0)
			{
				log("totalWeekRes Already Called .Returning Stored");
				return totalWeekRes;
			}
			else
			{
				totalWeekRes = $http.get(totalWeekDayWise);
				return totalWeekRes;
			}	
			
		},
			getTotalMappedFiles : function() 
		{
			if(getTotalMappedRes.length!=0)
			{
				log("getTotalMappedRes Already Called .Returning Stored");
				return getTotalMappedRes;
			}
			else
			{
				getTotalMappedRes = $http.get(getTotalMappedAPI);
				return getTotalMappedRes;
			}	
			
		}
		,
		getTotalAudioFiles : function(fileName)
		{
				
			var url = getAudioTextAPI +"?playFile="+fileName;
			log(url);
			var audioTextFile = $http.get(url);
				return audioTextFile;
				

		},

		getDayWiseSentiment : function()
		{

			if(totalDayWiseRes.length!=0)
			{
				return totalDayWiseRes;
			}
			else
			{
				totalDayWiseRes = $http.get(totalDayWiseAPI);
				return totalDayWiseRes;
			}	


		}


}
});



/*
getYearlyAgg : function()
		{
			if(yearly.length!=0)
			{	
			  log("Returning Stored Data For yearly Agg");
			  return yearly;
			}
			else
			{
			var d1 = Sentiments.getDaysAgo(365);
			var startDate =  Sentiments.formatDate(d1);
			var d2 = Sentiments.getDaysAgo(1);
			var endDate =  Sentiments.formatDate(d2);
			
			var params = {};
			params.startDate = startDate;
			params.endDate = endDate;

			var yearData = $http.get(yearlyAPI,{
				params :params
			});

				yearly=yearData;
				log("Returning New Data For yearly Agg");
				log(yearData);
				return yearData;
			}	
		}


*/