auth = angular.module("AuthenticationService",[]);

auth.service('Auth',function ($q,$window,$http,$rootScope) {
	var loginAPI =  Prod.host+ "api/auth";
	return {

		init : function(cred)
		{
			/*log("Authentication...Init");
			var isAuth = $http.post(loginAPI,{
				data: cred
			});*/
			 log(cred);
			 isAuth = $http.post(loginAPI, {username: cred.username, password: cred.password});

			return isAuth;

		}

	}
});