var call = angular.module("callDrivers",["ngRoute"]);

call.service('Call', function ($http,$q,$rootScope) 
{
    var callDriversRes = [];
    var callDriversAPI = Prod.host+ "api/callDriver";
   	return {
   		getCallDrivers : function()
	    {
	       if(callDriversRes.length!=0)
	       {
	       	return callDriversRes;
	       } 
	       else
	       {
	 		 callDriversRes  = $http.get(callDriversAPI);
	         return callDriversRes;
	       }
	    },
        getYearlyDate : function()
        {
            var params = {};
            var d1 = $.getDaysAgo(365);
            params.startDate = $.formatDate(d1);
            var d2 = $.getDaysAgo(0);
            params.endDate =  $.formatDate(d2);

            return params;

        },
        yearCallData : function()
        {


            var d1 = $.getDaysAgo(365);
            var startDate = $.formatDate(d1);
            var d2 = $.getDaysAgo(0);
            var endDate =  $.formatDate(d2);
            var yearapi =  Prod.host+"api/callDriver";
            var params = {};
            log("startDate",startDate);
            log("endDate",endDate);
            params.startDate = startDate;
            params.endDate = endDate;


            yesterdayTable  = $http.get(yearapi,
                {
                    params :params
                });
            return yesterdayTable;
        },
        weeklyCallData : function()
        {

                var d1 = $.getDaysAgo(7);
                var startDate = $.formatDate(d1);
                var d2 = $.getDaysAgo(0);
                var endDate =  $.formatDate(d2);
                var yearapi =  Prod.host+"api/callDriver";
                var params = {};
              //  params.type="week";
                params.startDate = startDate;
                params.endDate = endDate;


                weekly  = $http.get(yearapi,
                    {
                        params :params
                    });
                return weekly;

        },
        monthlyCallData : function()
        {
                var d1 = $.getDaysAgo(30);
                var startDate = $.formatDate(d1);
                var d2 = $.getDaysAgo(0);
                var endDate =  $.formatDate(d2);
                var yearapi =  Prod.host+"api/callDriver";
                var params = {};
                params.startDate = startDate;
                params.endDate = endDate;


                monthly  = $http.get(yearapi,
                    {
                        params :params
                    });
                return monthly;

        },
        yesterdayData : function()
        {

                var d1 = $.getDaysAgo(1);
                var startDate = $.formatDate(d1);
                var d2 = $.getDaysAgo(0);
                var endDate =  $.formatDate(d2);
                var yearapi =  Prod.host+"api/callDriver";
                var params = {};
                params.startDate = startDate;
                params.endDate = endDate;


                yesterday  = $http.get(yearapi,
                    {
                        params :params
                    });
                return yesterday;

        }





   	}
});  

