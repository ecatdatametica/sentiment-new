var sent = angular.module("sentAnalysis",["ngRoute"]);

sent.service('Sentiments', function ($http,$q,$rootScope) 
{
    var yesterday = [];
    var yesterdayTable = [];
    var weekly = [];
    var weeklyTable = [];
    var monthly = [];
    var monthlyTable =[];
    var yearly=[];
    var yearlyTable = [];
    return {

    getYesterdayDate : function()
    {
        var params = {};
        var d1 = $.getDaysAgo(1);
        params.startDate = $.formatDate(d1);
        var d2 = $.getDaysAgo(0);
        params.endDate =  $.formatDate(d2);

        return params;

    },

    yesterdayDataTable : function() 
    {
        
        if(yesterdayTable.length!=0)
        {   
        log("Returning Stored Data For Table Yesterday Agg");
        return yesterdayTable;
        }
        else
        {
        log("Called 1st Time Only For AJAX");
        var d1 = $.getDaysAgo(1);
        var startDate = $.formatDate(d1);
        var d2 = $.getDaysAgo(0);
        var endDate =  $.formatDate(d2);
        var yearapi =  Prod.host+"api/hourly";
        var params = {};
        params.dept_id ="table";
        params.type="yesterday";
        params.startDate = startDate;
        params.endDate = endDate;
        

        yesterdayTable  = $http.get(yearapi,
        {
        params :params
        });
        return yesterdayTable;
        }
    },




    yesterdayData : function() 
    {
        if(yesterday.length!=0)
        {   
        log("Returning Stored Data For Hourly Agg");
        return yesterday;
        }
        else
        {
        log("Called 1st Time Only For AJAX");
        var d1 = $.getDaysAgo(1);
        var startDate = $.formatDate(d1);
        var d2 = $.getDaysAgo(0);
        var endDate =  $.formatDate(d2);
        var yearapi =  Prod.host+"api/hourly";
        var params = {};
        params.type="yesterday";
        params.startDate = startDate;
        params.endDate = endDate;
        

        yesterday  = $http.get(yearapi,
        {
        params :params
        });
        return yesterday;
        }
    },


    yesterdayCustom: function(params) 
    {
        var yesterdayCustom;
        var d1 = $.getDaysAgo(1);
        var startDate = $.formatDate(d1);
        var d2 = $.getDaysAgo(0);
        var endDate =  $.formatDate(d2);
        var api =  Prod.host+"api/custom";;
       
        params.startDate = startDate;
        params.endDate = endDate;
        

        yesterdayCustom  = $http.get(api,
        {
        params :params
        });
        return yesterdayCustom;
        
    },

    yesterdayCustomDropDown: function(params) 
    {
        var yesterdayCustom;
        var d1 = $.getDaysAgo(1);
        var startDate = $.formatDate(d1);
        var d2 = $.getDaysAgo(0);
        var endDate =  $.formatDate(d2);
        var api =  Prod.host+"api/customDropDown";
       
        params.startDate = startDate;
        params.endDate = endDate;
        

        yesterdayCustom  = $http.get(api,
        {
        params :params
        });
        return yesterdayCustom;
        
    },
    /*Create Weekly Data*/

    getWeeklyDate : function()
    {
        var params = {};
        var d1 = $.getDaysAgo(7);
        params.startDate = $.formatDate(d1);
        var d2 = $.getDaysAgo(0);
        params.endDate =  $.formatDate(d2);

        return params;

    },

     weeklyData : function() 
    {
        if(weekly.length!=0)
        {   
        log("Returning Stored Data For Weekly Agg");
        return weekly;
        }
        else
        {
        log("Called 1st Time Only For AJAX");
        var d1 = $.getDaysAgo(7);
        var startDate = $.formatDate(d1);
        var d2 = $.getDaysAgo(0);
        var endDate =  $.formatDate(d2);
        var yearapi =  Prod.host+"api/hourly";
        var params = {};
        params.type="week";
        params.startDate = startDate;
        params.endDate = endDate;
        

        weekly  = $http.get(yearapi,
        {
        params :params
        });
        return weekly;
        }
    },

    weeklyDataTable : function() 
    {

        if(weeklyTable.length!=0)
        {   
        log("Returning Stored Data For Weekly Table Agg");
        return weeklyTable;
        }
        else
        {    
        log("Called 1st Time Only For AJAX");
        var d1 = $.getDaysAgo(7);
        var startDate = $.formatDate(d1);
        var d2 = $.getDaysAgo(0);
        var endDate =  $.formatDate(d2);
        var yearapi =  Prod.host+"api/hourly";
        var params = {};
        params.dept_id ="table";
        params.type="week";
        params.startDate = startDate;
        params.endDate = endDate;
        

        weeklyTable  = $http.get(yearapi,
        {
        params :params
        });
        return weeklyTable;
       } 
        
    },
    weeklyCustom: function(params) 
    {
        var weeklyCustom;
        var d1 = $.getDaysAgo(1);
        var startDate = $.formatDate(d1);
        var d2 = $.getDaysAgo(0);
        var endDate =  $.formatDate(d2);
        var api =  Prod.host+"api/custom";;
       
        params.startDate = startDate;
        params.endDate = endDate;
        

        weeklyCustom  = $http.get(api,
        {
        params :params
        });
        return weeklyCustom;
        
    },
     weeklyCustomDropDown: function(params) 
    {
        var weeklyCustom;
        var d1 = $.getDaysAgo(7);
        var startDate = $.formatDate(d1);
        var d2 = $.getDaysAgo(0);
        var endDate =  $.formatDate(d2);
        var api =  Prod.host+"api/customDropDown";
       
        params.startDate = startDate;
        params.endDate = endDate;
        

        weeklyCustom  = $http.get(api,
        {
        params :params
        });
        return weeklyCustom;
        
    },

    /*Get Monthly Data*/
    /*Create Weekly Data*/

    getMonthlyDate : function()
    {
        var params = {};
        var d1 = $.getDaysAgo(30);
        params.startDate = $.formatDate(d1);
        var d2 = $.getDaysAgo(0);
        params.endDate =  $.formatDate(d2);

        return params;

    },

     monthlyData : function() 
    {
        if(monthly.length!=0)
        {   
        log("Returning Stored Data For monthly Agg");
        return monthly;
        }
        else
        {
        log("Called 1st Time Only For AJAX");
        var d1 = $.getDaysAgo(30);
        var startDate = $.formatDate(d1);
        var d2 = $.getDaysAgo(0);
        var endDate =  $.formatDate(d2);
        var yearapi =  Prod.host+"api/hourly";
        var params = {};
        params.type="month";
        params.startDate = startDate;
        params.endDate = endDate;
        

        monthly  = $http.get(yearapi,
        {
        params :params
        });
        return monthly;
        }
    },

    monthlyDataTable : function() 
    {

        if(monthlyTable.length!=0)
        {   
        log("Returning Stored Data For Monthly Table Agg");
        return monthlyTable;
        }
        else
        {    
        log("Called 1st Time Only For AJAX");
        var d1 = $.getDaysAgo(30);
        var startDate = $.formatDate(d1);
        var d2 = $.getDaysAgo(0);
        var endDate =  $.formatDate(d2);
        var yearapi =  Prod.host+"api/hourly";
        var params = {};
        params.dept_id ="table";
        params.type="month";
        params.startDate = startDate;
        params.endDate = endDate;
        

        monthlyTable  = $http.get(yearapi,
        {
        params :params
        });
        return monthlyTable;
       } 
        
    },
    monthlyCustom: function(params) 
    {
        var monthlyCustom;
        var d1 = $.getDaysAgo(30);
        var startDate = $.formatDate(d1);
        var d2 = $.getDaysAgo(0);
        var endDate =  $.formatDate(d2);
        var api =  Prod.host+"api/custom";;
       
        params.startDate = startDate;
        params.endDate = endDate;
        

        monthlyCustom  = $http.get(api,
        {
        params :params
        });
        return monthlyCustom;
        
    },
     monthlyCustomDropDown: function(params) 
    {
        var monthlyCustom;
        var d1 = $.getDaysAgo(30);
        var startDate = $.formatDate(d1);
        var d2 = $.getDaysAgo(0);
        var endDate =  $.formatDate(d2);
        var api =  Prod.host+"api/customDropDown";
       
        params.startDate = startDate;
        params.endDate = endDate;
        

        monthlyCustom  = $http.get(api,
        {
        params :params
        });
        return monthlyCustom;
        
    },
    /*Gget yearly Data*/

    getYearlyDate : function()
    {
        var params = {};
        var d1 = $.getDaysAgo(365);
        params.startDate = $.formatDate(d1);
        var d2 = $.getDaysAgo(0);
        params.endDate =  $.formatDate(d2);

        return params;

    },

     yearlyData : function() 
    {
        if(yearly.length!=0)
        {   
        log("Returning Stored Data For Yearly Agg");
        return yearly;
        }
        else
        {
        log("Called 1st Time Only For AJAX");
        var d1 = $.getDaysAgo(365);
        var startDate = $.formatDate(d1);
        var d2 = $.getDaysAgo(0);
        var endDate =  $.formatDate(d2);
        var yearapi =  Prod.host+"api/hourly";
        var params = {};
        params.type="year";
        params.startDate = startDate;
        params.endDate = endDate;
        

        yearly  = $http.get(yearapi,
        {
        params :params
        });
        return yearly;
        }
    },

    yearlyDataTable : function() 
    {

        if(yearlyTable.length!=0)
        {   
        log("Returning Stored Data For yearlyTable Table Agg");
        return yearlyTable;
        }
        else
        {    
        log("Called 1st Time Only For AJAX");
        var d1 = $.getDaysAgo(365);
        var startDate = $.formatDate(d1);
        var d2 = $.getDaysAgo(0);
        var endDate =  $.formatDate(d2);
        var yearapi =  Prod.host+"api/hourly";
        var params = {};
        params.dept_id ="table";
        params.type="year";
        params.startDate = startDate;
        params.endDate = endDate;
        

        yearlyTable  = $http.get(yearapi,
        {
        params :params
        });
        return yearlyTable;
       } 
        
    },
    yearlyCustom: function(params) 
    {
        var yearlyCustom;
        var d1 = $.getDaysAgo(365);
        var startDate = $.formatDate(d1);
        var d2 = $.getDaysAgo(0);
        var endDate =  $.formatDate(d2);
        var api =  Prod.host+"api/custom";;
       
        params.startDate = startDate;
        params.endDate = endDate;
        

        yearlyCustom  = $http.get(api,
        {
        params :params
        });
        return yearlyCustom;
        
    },
     yearlyCustomDropDown: function(params) 
    {
        var yearlyCustom;
        var d1 = $.getDaysAgo(365);
        var startDate = $.formatDate(d1);
        var d2 = $.getDaysAgo(0);
        var endDate =  $.formatDate(d2);
        var api =  Prod.host+"api/customDropDown";
        params.startDate = startDate;
        params.endDate = endDate;
        

        yearlyCustom  = $http.get(api,
        {
        params :params
        });
        return yearlyCustom;
        
    }


};
});






/* getALLSentiments: function(api,params,callback) 
 {
            var cb = callback || angular.noop;
             if(sentiments.length !== 0) 
             {
                 cb(sentiments);
             }else
             {
                 $http.get(api,{
                      params :params
                      })
                     .success(function(result) 
                     {
               
                         sentiments = result;
                         cb(result);
                     })
                     .error(function() {
                         cb();
                     })
             }
       }*/