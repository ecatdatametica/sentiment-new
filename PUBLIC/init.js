
var timeStart =  Date.now();
var log ={};
log.enable = function()
{
	log = console.log.bind(console);
}

log.disable =  function()
{
	log = function(){};
}

var Prod = {};

Prod.enable = function()
{
	Prod.host = "http://114.143.131.83:3000/";
	console.log("Production Enables :"+Prod.host);
}

Prod.disable = function()
{
	Prod.host = "http://localhost:3000/";
	console.log("Production Disabled :"+Prod.host);

}


/*Log Properties*/
log.enable();
Prod.disable();

/*window.onload = function()
{
	angular.bootstrap(document,["myApp"]);
}
*/
var app = angular.module('myApp',["SpeechSentiment","AuthenticationService","callDrivers","ngRoute","ui.bootstrap","ngAnimate","sentAnalysis","smart-table","gridshore.c3js.chart","TopicSentiment","aggSentiment","ngAside"]);







app.config(function($routeProvider,$httpProvider)
{
		
	$httpProvider.defaults.useXDomain = true;
    delete $httpProvider.defaults.headers.common['X-Requested-With'];


	var timeEnd =  Date.now();
	//log("App Routing Ok..");
	//log("App Load Time : "+(timeEnd-timeStart)+" ms");
	$routeProvider
	.when("/",
	{
		templateUrl : "modals/loginPopUp/login.html",
		controller :  "loginCtrl"
	})
	.when("/landing",{
		templateUrl : "templates/landing.html",
		controller : "landingCtrl"
		})

	.when("/sentiment",{
		templateUrl : "templates/sentiment.html",
		controller :  "sentimentCtrl"
	})

	.when("/callDrivers",{
		templateUrl : "templates/callDrivers.html",
		controller :  "callDriversCtrl"
	})
	.when("/speechToText",{
		templateUrl : "templates/SpeechToTextTab.html",
		controller :  "speechToTextCtrl"
	})

	.when("/unauthorized",{
		templateUrl : "templates/unauthorized.html"
	})

	.otherwise({ redirectTo: '/unauthorized' });
});






