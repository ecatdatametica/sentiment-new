app.controller('landingCtrl',function ($location,$scope,$http,Sentiments,$rootScope,$modal,aggSent) {
  // refer to dayWise and sentiDay Ctrl and landing.html for controller mapping
    try
    {


        if($rootScope.isUserAuth)
        {



            $scope.sentDayWiseDisp = function()
            {
//Get Total Data
                aggSent.getTotalData().then(function(res)
                {
                    log("Total Data ");
                    log(res);
                    if(res.data.status)
                    {
                        $scope.totalData = res.data.data[0].total_count;


                        var pie = $.formatForPieCharts(res.data);
                        var donut = $.formatForDonutCharts(res.data);
                        donut.onclick = function (d, element) {
                            alert("Clicked" + d);
                            log(d);
                        }


                        donut.title ="Overall Data";
                        $.drawPie("landingPolarity",pie);


                        $.drawDonut("landingSentiments",donut);
                    }
                    else
                    {
                        $modal.open(
                            {
                                templateUrl : "modals/promptPopUp/promptPopUp.html",
                                controller : "promptPopUpCtrl",
                                keyboard:false,
                                backdrop:'static',
                                size:'lg'
                            });

                    }
                });

//Get Total Chat Data
                aggSent.getTotalChatData().then(function(res)
                {
                    if(res.data.status)
                    {
                        log("Total Data getTotalChatData");
                        log(res.data);
                        $scope.totalChat = res.data.data[0].total_count;
                        var pie = $.formatForPieCharts(res.data);
                        var donut = $.formatForDonutCharts(res.data);
                        donut.title ="Overall Chat Data";
                        $.drawDonut("chatPolarity",donut);
                        $.drawPie("chatSentiment",pie);
                    }
                    else
                    {
                        $modal.open(
                            {
                                templateUrl : "modals/promptPopUp/promptPopUp.html",
                                controller : "promptPopUpCtrl",
                                keyboard:false,
                                backdrop:'static',
                                size:'lg'
                            });

                    }

                });


                aggSent.getTotalEmailData().then(function(res)
                {
                    if(res.data.status)
                    {

                        log("Total Data getTotalEmailData");
                        log(res.data);
                        $scope.totalEmail = res.data.data[0].total_count;
                        var pie = $.formatForPieCharts(res.data);
                        var donut = $.formatForDonutCharts(res.data);
                        donut.title ="Overall Email Data";
                        $.drawDonut("emailPolarity",donut);
                        $.drawPie("emailSentiment",pie);
                    }
                    else
                    {
                        $modal.open(
                            {
                                templateUrl : "modals/promptPopUp/promptPopUp.html",
                                controller : "promptPopUpCtrl",
                                keyboard:false,
                                backdrop:'static',
                                size:'lg'
                            });

                    }
                });

                aggSent.getTotalVoiceData().then(function(res)
                {
                    if(res.data.status)
                    {
                        log("Total Data getTotalEmailData");
                        log(res.data);
                        $scope.totalVoice = res.data.data[0].total_count;
                        var pie = $.formatForPieCharts(res.data);
                        var donut = $.formatForDonutCharts(res.data);
                        donut.title ="Overall Voice Data";
                        $.drawDonut("voicePolarity",donut);
                        $.drawPie("voiceSentiment",pie);
                    }
                    else
                    {
                        $modal.open(
                            {
                                templateUrl : "modals/promptPopUp/promptPopUp.html",
                                controller : "promptPopUpCtrl",
                                keyboard:false,
                                backdrop:'static',
                                size:'lg'
                            });

                    }
                });
                aggSent.getTotalWeekDayWise().then(function(res)
                {
                    if(res.data.status)
                    {
                        log("Total Data getTotalWeekDayWise");
                        log(res.data);
                        $('#weekDayAnger div,#weekDayAnticipation div,#weekDayDisgust div,#weekDayFear div,#weekDayJoy div,#weekDaySadness div,#weekDaySurprise div,#weekDayTrust div').remove();
                        $('#weekDayAnger').append('<div id="weekDayAnger1"></div>');
                        $('#weekDayAnticipation').append('<div id="weekDayAnticipation1"></div>');
                        $('#weekDayDisgust').append('<div id="weekDayDisgust1"></div>');
                        $('#weekDayFear').append('<div id="weekDayFear1"></div>');
                        $('#weekDayJoy').append('<div id="weekDayJoy1"></div>');
                        $('#weekDaySadness').append('<div id="weekDaySadness1"></div>');
                        $('#weekDaySurprise').append('<div id="weekDaySurprise1"></div>');
                        $('#weekDayTrust').append('<div id="weekDayTrust1"></div>');


                        var anger = $.formatForDayWise(res.data,"Anger");
                        $.drawDonut("weekDayAnger1",anger);

                        var Anticipation = $.formatForDayWise(res.data,"Anticipation");
                        $.drawDonut("weekDayAnticipation1",Anticipation);

                        var Disgust = $.formatForDayWise(res.data,"Disgust");
                        $.drawDonut("weekDayDisgust1",Disgust);

                        var Fear = $.formatForDayWise(res.data,"Fear");
                        $.drawDonut("weekDayFear1",Fear);

                        var Joy = $.formatForDayWise(res.data,"Joy");
                        $.drawDonut("weekDayJoy1",Joy);

                        var Sadness = $.formatForDayWise(res.data,"Sadness");
                        $.drawDonut("weekDaySadness1",Sadness);

                        var Surprise = $.formatForDayWise(res.data,"Surprise");
                        $.drawDonut("weekDaySurprise1",Surprise);

                        var Trust = $.formatForDayWise(res.data,"Trust");
                        $.drawDonut("weekDayTrust1",Trust);




                    }
                    else
                    {
                        $modal.open(
                            {
                                templateUrl : "modals/promptPopUp/promptPopUp.html",
                                controller : "promptPopUpCtrl",
                                keyboard:false,
                                backdrop:'static',
                                size:'lg'
                            });

                    }
                });
                $scope.flag=true;
                $scope.loadDayWise = function()
                {
//get Day Wise Data
                    if($scope.flag==true){
                        $scope.flag=false;
                    aggSent.getDayWiseSentiment().then(function(res) {
                        log("DAY WISE FOR TAB2 REPRESENTATION");
                      /*  $('#dayWise10,#dayWise11,#dayWise12,#dayWise13,#dayWise14,#dayWise15,#dayWise16').remove();
                        $('#dayWise0').append('<div id="dayWise10"></div>');
                        $('#dayWise1').append('<div id="dayWise11"></div>');
                        $('#dayWise2').append('<div id="dayWise12"></div>');
                        $('#dayWise3').append('<div id="dayWise13"></div>');
                        $('#dayWise4').append('<div id="dayWise14"></div>');
                        $('#dayWise5').append('<div id="dayWise15"></div>');
                        $('#dayWise6').append('<div id="dayWise16"></div>');*/
                        var i=0;
                        $scope.title1 = res.data.data[0].dayname;
                        var resData = res.data.data[0];
                        var pie = $.formatForDayWiseSent("donut",$scope.title1,resData);
                        var id = "dayWise10";
                        $.drawDonut(id,pie);
                        i+=1;
                        $scope.title2 = res.data.data[1].dayname;
                        var resData = res.data.data[2];
                        var pie = $.formatForDayWiseSent("donut",$scope.title2,resData);
                        var id = "dayWise11";
                        $.drawDonut(id,pie);
                        i+=1;
                        $scope.title3 = res.data.data[2].dayname;
                        var resData = res.data.data[2];
                        var pie = $.formatForDayWiseSent("donut",$scope.title3,resData);
                        var id = "dayWise12";
                        $.drawDonut(id,pie);
                        i+=1;
                        $scope.title4 = res.data.data[3].dayname;
                        var resData = res.data.data[3];
                        var pie = $.formatForDayWiseSent("donut",$scope.title4,resData);
                        var id = "dayWise13";
                        $.drawDonut(id,pie);
                        i+=1;
                        $scope.title5 = res.data.data[4].dayname;
                        var resData = res.data.data[4];
                        var pie = $.formatForDayWiseSent("donut",$scope.title5,resData);
                        var id = "dayWise14";
                        $.drawDonut(id,pie);
                        i+=1;
                        $scope.title6 = res.data.data[5].dayname;
                        var resData = res.data.data[5];
                        var pie = $.formatForDayWiseSent("donut",$scope.title6,resData);
                        var id = "dayWise15";
                        $.drawDonut(id,pie);
                        i+=1;
                        $scope.title7 = res.data.data[6].dayname;
                        var resData = res.data.data[6];
                        var pie = $.formatForDayWiseSent("donut",$scope.title7,resData);
                        var id = "dayWise16";
                        $.drawDonut(id,pie);

                    });
                    }
                }


            }

            /*Call OnLAoad */
            $scope.sentDayWiseDisp();


        }
        else
        {
            $location.path("/unauthorized");
        }




    }
    catch(e)
    {
        $modal.open(
            {
                templateUrl : "modals/promptPopUp/promptPopUp.html",
                controller : "promptPopUpCtrl",
                keyboard:false,
                backdrop:'static',
                size:'lg'
            });



    }
});





/*


log("Default Scope Data");
	//Refer To sentimentServices.js Services Under Services Folder
	var d1 = Sentiments.getDaysAgo(1);
	var startDate =  Sentiments.formatDate(d1);
	var d2 = Sentiments.getDaysAgo(0);
	var endDate =  Sentiments.formatDate(d2);
	log(startDate + " "+endDate);
	var api = "http://localhost:3000/api/sentiment";
	var params = {};
	params.startDate = startDate;
	params.endDate = endDate;
	Sentiments.getALLSentiments(api,params,function(res) 
	{


		if(res.status==true)
		{	

		var sort = Sentiments.sortBy(res.data,"file_ts");
		//Above FUnction is to Order By Timestamp column name file_ts
		//If Already ordered Then No Need to use This Function	
		log(sort);
		var date = Sentiments.getDates(sort);
		$scope.startDate  = date.startDate;
		$scope.endDate  = date.endDate;

		

		log("Sorting Value Based On file_ts");	
		$scope.sentiDispData = sort;
		$scope.showTable=true;
		$scope.stSafeSenti = sort;

		//get Sum OF ALL Values
		var anger   =Sentiments.getDataByField(sort,"Anger");
		var disgust =Sentiments.getDataByField(sort,"Disgust");
		var fear    = Sentiments.getDataByField(sort,"Fear");
		var joy     = Sentiments.getDataByField(sort,"Joy");
		var sadness = Sentiments.getDataByField(sort,"Sadness");
		var surprise = Sentiments.getDataByField(sort,"Surprise");
		var polNeg =  Sentiments.getDataByField(sort,"polnegpercent");
		var polPos  = Sentiments.getDataByField(sort,"polpospercent");


	
		//Draw Aggrevated Pie Chart For ALL Data
		var pieData ={};
		pieData.columns = [];
		pieData.columns.push(anger.arrayList);
		pieData.columns.push(disgust.arrayList);
		pieData.columns.push(fear.arrayList);
		pieData.columns.push(joy.arrayList);
		pieData.columns.push(sadness.arrayList);
		pieData.columns.push(surprise.arrayList);
		pieData.type="pie";

		$.drawPie("aggPieChart",pieData);

		
		var donutData =[];
		donutData.push(anger);
		donutData.push(disgust);
		donutData.push(fear);
		donutData.push(joy);
		donutData.push(sadness);
		donutData.push(surprise);

		var formDonut = Sentiments.formatDonutData(donutData);
		formDonut.title ="Average Sentiment Overall";
		$.drawDonut("donutChart",formDonut);

		
		var polChart = [];
		polChart.push(polNeg);
		polChart.push(polPos);

		var polDont = Sentiments.formatDonutData(polChart);
		polDont.title ="Combined Polarity";
		$.drawDonut("polarityChart",polDont);



	

		var timeSeries ={};
		timeSeries.columns = [];
		timeSeries.columns.push(anger);
		timeSeries.columns.push(disgust);
		timeSeries.columns.push(fear);
		timeSeries.columns.push(joy);
		timeSeries.columns.push(sadness);
		timeSeries.columns.push(surprise);

		log("Time Series Data");
		log(timeSeries);
		log("Complete Date List");
		log(date);


		var timeFormattedData = Sentiments.formatChartTimeSeries(timeSeries,date);
		$.drawTimeSeries("graphALLChart",timeFormattedData);
		
		}
		else
		{
		 log("Server Data Not Available");
		}


		//Create Charts For Data
		}); 

*/