app.controller('yesterdayCtrl', function ($location, $modal, $scope, $http, Sentiments, $rootScope, $location, aggSent) {

    if ($rootScope.isUserAuth) {


        log("loaded yesterday");
        $scope.tableBreakDown = 0;
        $scope.date = Sentiments.getYesterdayDate();


        log("yesterdayCtrl Executing..view Content Loaded");

//Default Values
        $scope.dropDownDepart = "Filter By Department";
        $scope.dropDownCat = "Filter By Category";
        $scope.dropDownRep = "Filter By Representative"
        $scope.dropDownCh = "Filter By Channel";

        $scope.status =
        {
            isopen: false
        };

        $scope.toggled = function (open) {
            log('Dropdown is now: ', open);
        };

        $scope.toggleDropdown = function ($event) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.status.isopen = !$scope.status.isopen;
        };


        $scope.getChoiceDepart = function (val) {
            $scope.dropDownDepart = val;
        }
        $scope.getChoiceCat = function (val) {
            $scope.dropDownCat = val;
        }
        $scope.getChoiceRep = function (val) {
            $scope.dropDownRep = val;
        }
        $scope.getChoiceCh = function (val) {
            $scope.dropDownCh = val;
        }


        $scope.loadYes = function () {

            $scope.filterText = "Hourly Representation";
            var t = Sentiments.yesterdayData().then(function (res) {


                if (res.data.status) {
                    log("Total Data For Yesterday :");

                    log(res.data);
                    //filter data by Department By
                    $scope.filteredData = $.performFilter(res.data);
                    //Get Unique keyWords For Data
                    $scope.department = $.getUniqueDept(res.data);
                    $scope.uniqueRep = $.getUniqueRep(res.data);
                    $scope.category = $.getUniqueCategory(res.data);
                    $scope.uniqueChan = $.getUniqueChannel(res.data);
                    $scope.avgCount = res.data.data.length;
                    /*log("table Data :");
                     log(res.data);
                     $scope.sentiDispData = res.data.data;
                     $scope.stSafeSenti = res.data.data;*/


                    Sentiments.yesterdayDataTable().then(function (res) {

                        log("table Data :");
                        log(res.data);
                        $scope.sentiDispData = res.data.data;
                        $scope.stSafeSenti = res.data.data;

                        $scope.TotalCount = res.data.data.length;
                        $scope.tableBreakDown = res.data.data.length;

                        $scope.drillfinaldata = res.data.data;
                       // $rootScope.sentiDispDataDrilled = res.data.data;
                       // $rootScope.stSafeSentiDrilled = res.data.data;

                    });


                    var data = $.formatForStackedCharts(res.data);
                    var time = $.formatForTimeSeries(res.data);
                    var pie = $.formatForPieCharts(res.data);
                    var donut = $.formatForDonutCharts(res.data);

                    pie.onclick = function (name, element) {

                        $rootScope.clickedType = "Yesterday Drill Down -" +name.id;
                       $rootScope.sentiDispDataDrilled=[]
                        $rootScope.stSafeSentiDrilled=[]
                        $rootScope.valuesort=name.id
                        angular.forEach($scope.drillfinaldata,function(thisdata){
                            Object.keys(thisdata).forEach(function (key){
                               if(key == name.id){
                                   if(thisdata[key] > 0){
                                       $rootScope.sentiDispDataDrilled.push(thisdata)
                                       $rootScope.stSafeSentiDrilled.push(thisdata)

                                   }
                               }
                            });

                        })

                        log($rootScope.sentiDispDataDrilled)

                    //    $rootScope.clickedData = data;
                       // log($rootScope.clickedData.id )
                        $rootScope.clickedElement = element;
                        $modal.open(
                            {
                                templateUrl: "modals/drillDown/drillDown.html",
                                controller: "drillDownCtrl",
                                keyboard: false,
                                backdrop: 'static',
                                size: 'lg'
                            });
                    }


                    //	$.drawStackedBar("graphALLChart",data);
                    //	$.drawSpline("graphALLChart",data);
                    $.drawTimeSeries("graphALLChart", time, "yesterday");
                    $.drawDonut("aggPieChart", pie);
                    $.drawPie("polarityChart", donut);

                }
                else {
                    $scope.alertShow = true;
                    $scope.alertType = "error";
                    $scope.alertMessage = "Data Not Available";
                    $rootScope.ErrorText = "Data Not Available";
                    log("Data Not Available");
                    $modal.open(
                        {
                            templateUrl: "modals/promptPopUp/promptPopUp.html",
                            controller: "promptPopUpCtrl",
                            keyboard: false,
                            backdrop: 'static',
                            size: 'lg'
                        });
                }
            });
                     /*Get By Default For Yesteray Data For Sentiments*/

                    // /}  //yesterday Ends



            $scope.performFilter = function () {

                /*if($scope.dropDownDepart !="Filter By Department" || $scope.dropDownCat !=  "Filter By Category" || $scope.dropDownRep != "Filter By Representative" || $scope.dropDownCh != "Filter By Channel")
                 {
                 */

                var params = {};
                params.dept_id = $scope.dropDownDepart;
                params.category_id = $scope.dropDownCat;
                params.rep_id = $scope.dropDownRep;
                params.channel = $scope.dropDownCh;
                log('Selected Params');
                log(params);
                Sentiments.yesterdayCustom(params).then(function (res) {


                    log("Values Based On Selected Values");
                    log($scope.dropDownDepart + " " + $scope.dropDownCat + " " + $scope.dropDownRep + " " + $scope.dropDownCh);
                    log(res);
                    if (res.data.status) {

                        $scope.filterText = "Based On Filter :";
                        if (params.dept_id != "Filter By Department") {
                            $scope.filterText += " Department,";
                        }

                        if (params.category_id != "Filter By Category") {
                            $scope.filterText += "Category,";

                        }

                        if (params.rep_id != "Filter By Representative") {
                            $scope.filterText += "Representative,";
                        }

                        if (params.channel != "Filter By Channel") {
                            $scope.filterText += "Channel,";
                        }

                        Sentiments.yesterdayCustomDropDown(params).then(function (res) {
                            log("table Data For Customized Filter:");
                            log(res.data.data);
                            $scope.sentiDispData = res.data.data;
                            $scope.stSafeSenti = res.data.data;
                            $scope.tableBreakDown = res.data.data.length;


                        });


                        $scope.avgCount = res.data.data.length;
                        var pie = $.formatForPieCharts(res.data);
                        var donut = $.formatForDonutCharts(res.data);
                        var time = $.formatForTimeSeries(res.data);
                    //var data = $.formatForAreaChart(res.data);

                        /*log("Table Data Based On Filters:");
                         log(res.data);
                         $scope.sentiDispData = res.data.data;
                         $scope.stSafeSenti = res.data.data;*/

                        log("table Data :");
                        log(  $scope.sentiDispData );
                        $scope.sentiDispData = res.data.data;
                        $scope.stSafeSenti = res.data.data;

                        $scope.count = res.data.data.length;


                //$.drawStackedBar("graphALLChart",data);
                    //	$.drawSpline("graphALLChart",data);

                        pie.onclick = function (name, element) {
                            log(name)
                            $rootScope.sentiDispDataDrilled=[]
                            $rootScope.stSafeSentiDrilled=[]
                            $rootScope.valuesort=name.id
                            angular.forEach($scope.drillfinaldata,function(thisdata){
                                Object.keys(thisdata).forEach(function (key){
                                    if(key == name.id){
                                        if(thisdata[key] > 0){
                                            $rootScope.sentiDispDataDrilled.push(thisdata)
                                            $rootScope.stSafeSentiDrilled.push(thisdata)

                                        }
                                    }
                                });

                            })

                            log($rootScope.sentiDispDataDrilled)
                            $rootScope.clickedType = "yesterday";
                         //   $rootScope.clickedData = data;
                            $rootScope.clickedElement = element;
                            $modal.open(
                                {
                                    templateUrl: "modals/drillDown/drillDown.html",
                                    controller: "drillDownCtrl",
                                    keyboard: false,
                                    backdrop: 'static',
                                    size: 'lg'
                                });
                        }
                        $.drawTimeSeries("graphALLChart", time, "yesterday");
                        $.drawDonut("aggPieChart", pie);
                        $.drawPie("polarityChart", donut);

                    }
                    else {
                        $rootScope.ErrorText = "Data Not Available";
                        $modal.open(
                            {
                                templateUrl: "modals/promptPopUp/promptPopUp.html",
                                controller: "promptPopUpCtrl",
                                keyboard: false,
                                backdrop: 'static',
                                size: 'lg'
                            });

                    }


                });
                //get all data based on Variable Fields
                                /* }
                                 else
                                 {
                                 $modal.open(
                                 {
                                 templateUrl : "modals/loginPop/login.html",
                                 controller : "loginCtrl",
                                 keyboard:false,
                                 backdrop:'static',
                                 size:'lg'
                                 });

                                 }*/
            };


            $scope.resetFilter = function () {
                $scope.dropDownDepart = "Filter By Department";
                $scope.dropDownCat = "Filter By Category";
                $scope.dropDownRep = "Filter By Representative"
                $scope.dropDownCh = "Filter By Channel";
                var t = Sentiments.yesterdayData().then(function (res) {
                    if (res.data.status) {
                        $scope.filterText = " For All Category Based On Per Hourly Avg";
                        $scope.avgCount = res.data.data.length;
                        log("Total Data For Yesterday :");
                        log(res.data);
                //filter data by Department By
                        $scope.filteredData = $.performFilter(res.data);
                //Get Unique keyWords For Data
                        $scope.department = $.getUniqueDept(res.data);
                        $scope.uniqueRep = $.getUniqueRep(res.data);
                        $scope.category = $.getUniqueCategory(res.data);
                        $scope.uniqueChan = $.getUniqueChannel(res.data);

                        log("table Data :");
                        log(res.data);
                        $scope.sentiDispData = res.data.data;
                        $scope.stSafeSenti = res.data.data;


                        Sentiments.yesterdayDataTable().then(function (res) {

                            log("table Data :");
                            log(res.data);
                            $scope.sentiDispData = res.data.data;
                            $scope.stSafeSenti = res.data.data;

                            $scope.count = res.data.data.length;

                        });


                    //var data = $.formatForAreaChart(res.data);
                        var time = $.formatForTimeSeries(res.data);
                        var pie = $.formatForPieCharts(res.data);
                        var donut = $.formatForDonutCharts(res.data);


                        //	$.drawStackedBar("graphALLChart",data);
                    //	$.drawSpline("graphALLChart",data);
                        $.drawTimeSeries("graphALLChart", time, "yesterday");
                        $.drawDonut("aggPieChart", pie);
                        $.drawPie("polarityChart", donut);
                    }
                    else {
                        $scope.alertShow = true;
                        $scope.alertType = "error";
                        $scope.alertMessage = "Data Not Available";
                        $rootScope.ErrorText = "Data Not Available";
                        log("Data Not Available");
                        $modal.open(
                            {
                                templateUrl: "modals/promptPopUp/promptPopUp.html",
                                controller: "promptPopUpCtrl",
                                keyboard: false,
                                backdrop: 'static',
                                size: 'lg'
                            });
                    }
                });


            }  //filter Ends


        }
        $scope.loadYes();

    }
    else {
        $location.path("/unauthorized");
    }


});