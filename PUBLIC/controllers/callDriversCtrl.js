app.controller('callDriversCtrl', function (Call,$q,$rootScope,$scope,$location,Topic,Sentiments) 
{
  if($rootScope.isUserAuth)
  {

      $rootScope.alertShow=false;
      /*Call.getCallDrivers().then(function(res)
      {
        log("Call Drivers Data");
        log(res.data.data);
        log(JSON.stringify(res.data.data));
        var data  = res.data.data;
        // var list = $.filterModellingData(res.data);
        //Data already Filtered and Modellied on the Bsvckend for this REST CALL.
       var holder = $('#hierarchical-pie-demo-1');
        holder.empty();

        new HierarchicalPie
        ({
        chartId : '#hierarchical-pie-demo-1',
        data : data,
        legendContainer : '#pie-chart-legend-1',
        navigation      : '#chart-navigator-1'
        });
         window.prettyPrint && prettyPrint();
         

      });*/
      $scope.yearCallDriverFunc=function(){
         // $scope.date = Call.getYearlyDate();
          $('.yearhierarchical div').remove()
          $('.yearhierarchical').append('<div id="yearhierarchical-pie-demo-1"></div>')
          Call.yearCallData().then(function(res){
              log(res.data)
              new HierarchicalPie
              ({
                  chartId : '#yearhierarchical-pie-demo-1',
                  data : res.data.data,
                  legendContainer : '#pie-chart-legend-1',
                  navigation      : '#chart-navigator-1'
              });
          });

      }
      $scope.weeklyCallDriverFunc=function(){
          // $scope.date = Call.getYearlyDate();
          $('.weeklyhierarchical div').remove()
          $('.weeklyhierarchical').append('<div id="weeklyhierarchical-pie-demo-1"></div>')
          Call.weeklyCallData().then(function(res){
              log(res.data)
              new HierarchicalPie
              ({
                  chartId : '#weeklyhierarchical-pie-demo-1',
                  data : res.data.data,
                  legendContainer : '#weeklypie-chart-legend-1',
                  navigation      : '#weeklychart-navigator-1'
              });
          });

      }

      $scope.yesterdayCallDriverFunc=function(){
          // $scope.date = Call.getYearlyDate();
          $('#yesterdayhierarchical-pie-demo-1 svg').remove()

          Call.yesterdayData().then(function(res){
              log(res.data)
              new HierarchicalPie
              ({
                  chartId : '#yesterdayhierarchical-pie-demo-1',
                  data : res.data.data,
                  legendContainer : '#yesterdaypie-chart-legend-1',
                  navigation      : '#yesterdaychart-navigator-1'
              });
          });

      }
      $scope.yesterdayCallDriverFunc();
      $scope.monthlyCallDriverFunc=function(){
          // $scope.date = Call.getYearlyDate();
          $('.monthlyhierarchical div').remove()
          $('.monthlyhierarchical').append('<div id="monthlyhierarchical-pie-demo-1"></div>')
          Call.monthlyCallData().then(function(res){
              log(res.data)
              new HierarchicalPie
              ({
                  chartId : '#monthlyhierarchical-pie-demo-1',
                  data : res.data.data,
                  legendContainer : '#monthlypie-chart-legend-1',
                  navigation      : '#monthlychart-navigator-1'
              });
          });

      }
     // $scope.weeklyCallDriverFunc();




/*	$(function() 
  {
  var holder = $('#hierarchical-pie-demo-1');
  holder.empty();

  $.getJSON('Data/sample-data-1.json', function(data) 
  {
  
  new HierarchicalPie
  ({
  chartId : '#hierarchical-pie-demo-1',
  data : data,
  legendContainer : '#pie-chart-legend-1',
  navigation      : '#chart-navigator-1'
  });
  
  });

  window.prettyPrint && prettyPrint();
  });
*/

  }
  else
  {
    $location.path("/unauthorized");
  }

});





/*
$.filter = function(res) 
{    
        var groups = {all:{}};
    res.forEach(function(val)
    {
      
      if(!groups[val.secondary_topic])
        { 
          groups[val.secondary_topic] = {};
      }
    
      groups[val.secondary_topic]['name']=val.secondary_topic;
      groups[val.secondary_topic][val.modelled_topic] = {};
      groups[val.secondary_topic][val.modelled_topic]["count"] = val.count;
      groups[val.secondary_topic]['count'] = groups[val.secondary_topic]['count'] + val.count;
  // /      groups[val.secondary_topic][val.modelled_topic] = groups["all"][val.modelled_topic] = val.count;

    });




    return groups;
};

BELOW ORIGINAL

$.filter = function(res) 
{    
        var groups = {all:{}};
    res.forEach(function(val)
    {
      if(!groups[val.secondary_topic])
        { 
          groups[val.secondary_topic] = {};
      }
      log(val.count);
      groups[val.secondary_topic][val.modelled_topic] = groups["all"][val.modelled_topic] = val.count;
     // groups[val.primary_topic][val.secondary_topic] [val.modelled_topic] = groups["all"][val.modelled_topic] = 1;
    });


    
    
    return groups;
};
*/


/*
WORKING DATA MODELLING
$.filter = function(res) 
{    
        var groups = {};
    res.forEach(function(val)
    {
      if(!groups[val.primary_topic])
      {
            groups[val.primary_topic] = {};
          groups[val.primary_topic]['name'] = val.primary_topic;
          groups[val.primary_topic]['count'] = 0;
      }
      if(!groups[val.primary_topic][val.secondary_topic])
        { 
          groups[val.primary_topic][val.secondary_topic] = {};
          groups[val.primary_topic][val.secondary_topic]['name'] = val.secondary_topic;
          groups[val.primary_topic][val.secondary_topic]['count'] = 0;
      }
      log(val.count);
      if (!groups[val.primary_topic][val.secondary_topic][val.modelled_topic])
      {
        groups[val.primary_topic][val.secondary_topic][val.modelled_topic] = {};
      }
      //groups[val.secondary_topic]['name'] = val.secondary_topic;
      groups[val.primary_topic][val.secondary_topic]['count'] += val.count;
      groups[val.primary_topic]['count'] += val.count;
      groups[val.primary_topic][val.secondary_topic][val.modelled_topic]['name']  = val.modelled_topic;
      groups[val.primary_topic][val.secondary_topic][val.modelled_topic]['count']  = val.count;
     // groups[val.primary_topic][val.secondary_topic] [val.modelled_topic] = groups["all"][val.modelled_topic] = 1;
    });




    return groups;
};




*/