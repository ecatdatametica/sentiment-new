app.controller('speechToTextCtrl', function (Speech,$location,$http,$rootScope,$scope,$location,aggSent,$modal, $timeout) {

    if($rootScope.isUserAuth)
    {



        $rootScope.showPause=true;
        $rootScope.hideLoader=true;


        var Notification = window.Notification || window.mozNotification || window.webkitNotification;

        Notification.requestPermission(function (permission)
        {
            // console.log(permission);
        });


        $rootScope.pauseAudio = function()
        {
            var audio =document.getElementById("audioPlayer");
            audio.pause();
            $rootScope.showPlay=true;
            $rootScope.showPause=false;


        }
        $rootScope.playAudio = function()
        {
            var audio =document.getElementById("audioPlayer");
            audio.play();
            $rootScope.showPlay=false;
            $rootScope.showPause=true;

        }



        $rootScope.show = function(value,body)
        {
            var options =
            {
                body: body,
                icon: "images/images.jpg",
                tag : "custom"
            };
            //tag : "custom"
            //  dir : "ltr"
            $rootScope.instance = new Notification("Current Playing",options);

            $rootScope.instance.onclick = function () {
                var audio =document.getElementById("audioPlayer");
                audio.pause();
                // Something to do
            };
            $rootScope.instance.onerror = function () {
                // Something to do
            };
            $rootScope.instance.onshow = function () {
                // Something to do
            };
            $rootScope.instance.onclose = function () {
                $rootScope.pauseAudio();

                // Something to do
            };

            return false;
        }

        var textFile ={};
        textFile.audioText = [];
        $scope.audioResText = "No Audio File Selected";

        $scope.audioallData=function(){
            aggSent.getTotalMappedFiles().then(function(res)
            {
                log("TOTAL COMBINED FILES");
                log(res.data);
                $scope.data= res.data;
                $scope.audio= res.data.audio;
                $scope.audioText = res.data.audioText;


                var i=0;
                while(i!=res.data.audioText.length)
                {
                    var file ={};

                    var splitText = res.data.audioText[i].split("-");
                    log(splitText);
                    file.fullPath = res.data.audioText[i];
                    file.splitName = splitText[0];
                    textFile.audioText.push(file);
                    i+=1;
                }
                log(textFile);


            });
        }
        $scope.audioallData();

/*        $rootScope.getPlay = function(clickfile)
        {
            $scope.isPlaying = true;
            log(clickfile)

            $scope.currentPlaying = clickfile;
            Speech.getSpeechSentiments(clickfile).then(function(res)
            {
                log("Speech Sentiments : "+clickfile);
                if(res.data.status)
                {
                    log("Got Some Data");
                    var resDataAudio = res.data;
                    log(resDataAudio);
                    var pie = $.formatForPieCharts(resDataAudio);
                    var id = "audioSentiments";
                    $.drawPie(id,pie);
                    $scope.sentAudioFileName =clickfile.substring(0,10);;
                    var pola =  $.formatForDonutCharts(resDataAudio);
                    log("POLARITY DATA : ");
                    log(pola);
                    $.drawDonut("audioPolarity",pola);
                    // /
                }

            });*/

        $rootScope.getPlay = function(clickfile, indx)
        {
            $scope.isPlaying = true;
            log(clickfile)
            $scope.currentPlaying = clickfile;
            Speech.getSpeechSentiments(clickfile).then(function(res)
            {
                log("Speech Sentiments : "+clickfile);
                if(res.data.status)
                {
                    log("Got Some Data");

                    var resDataAudio = res.data;
                    log(resDataAudio);
                    var pie = $.formatForPieCharts(resDataAudio);
                    var id = "audioSentiments";

                    var texts = $.attachTextToPie(indx);
                    //$.drawPie(id,pie);
                    $.drawPiewithWords(id,pie,texts);
                    $scope.sentAudioFileName =clickfile.substring(0,10);
                    var pola =  $.formatForDonutCharts(resDataAudio);

                    console.log(JSON.stringify(pola));
                    log("POLARITY DATA : ");
                    log(pola);
                    var texts = $.attachTextToDonut(indx);
                    $.drawdonutwithWords("audioPolarity",pola,texts);
                    // /
                }
            });



            var url = Prod.host+"audio?playFile="+clickfile+"&token="+$rootScope.token;

            $rootScope.show("Currenty Playing :",clickfile);

            var split = clickfile.split("-")||clickfile.split(" ");
            var audio = document.getElementById("audioPlayer");
            audio.src= url;
            audio.play();
            $rootScope.totalDuration = audio.duration;


            var i=0;
            log(textFile.audioText.length);
            while(i!=textFile.audioText.length)
            {
                log(textFile);
                log(textFile.audioText[i].splitName);

                if(textFile.audioText[i].splitName==split[0])
                {
                    log("Matched Text File");
                    log("Text File  : "+textFile.audioText[i].fullPath);
                    log("Audio File : "+clickfile);
                    //get Audio text Files

                    var audioFilePath = textFile.audioText[i].fullPath;
                    $scope.currentPlayingAudio = audioFilePath;
                    aggSent.getTotalAudioFiles(audioFilePath).then(function(res)
                    {
                        log(res);
                        $scope.audioResText = res.data;


                        var splitTextFile = res.data.split(" ");
                        /*	var i =0;
                         while(i!=splitTextFile.length)
                         {

                         i+=1;
                         }
                         */

                    });

                }

                i+=1;
            }
        }



        $scope.errordata=true
        $scope.searchAudio = function(search) {
            //Send AJAX QUERY FOR SEARCH RESULTS
            if (search != undefined || search.length != 0) {
                aggSent.getAudioListBySearch(search).then(function (res) {


                    log($scope.data)

                    if(res.data.audio.length==0){
                        $scope.errordata=false
                    }
                    $scope.data = res.data

                    //
                });
            }
            else {

            }
        }
        $scope.doSearch=function(search){

            if(search.length==0){
                $scope.audioallData();
                $scope.errordata=true
            }

        }

        $scope.RenderFunction= function(){

            var chart = c3.generate({
                bindto: '#AmountUser',
                legend: {
                    position: 'right'
                },
                data: {
                    columns: [
                        ['Purchases', 65],
                        ['Currently in cart', 34],
                        ['Added but abandoned', 12]
                    ],
                    type : 'pie'


                },
                pie:{
                    label:{
                        format : function(value){
                            return value;
                        }
                    }
                },
                size: { width: 350, height:280 }
            });
        }


        $scope.loadProfile = function(){
            $modal.open(
                {
                    templateUrl: "modals/UserProfile/UserProfile.html",
                    controller: "userProfileCntrl",
                    keyboard: false,
                    backdrop: 'static',
                    size: 'lg'
                });


            /*$timeout(function () {
             modalInstance.close('closing');
             }, 2000);*/
        };



        $scope.loadmail = function(){
            $modal.open(
                {
                    templateUrl: "modals/promptPopUp/Mail.html",
                    controller: "promptPopUpCtrl",
                    keyboard: false,
                    backdrop: 'static',
                    size: 'lg'
                });


            /*$timeout(function () {
             modalInstance.close('closing');
             }, 2000);*/
        };


        $scope.loadSummaryProfile = [];
        $scope.LatestIteractions = [];
        $scope.LatestOrders = [];
        $scope.selectedCustomerProfileData = [];

        $scope.assignDummyData = function(){
            $scope.loadSummaryProfile.push({
                'Id'                 :206408018820140602,
                'Name'               :'John Rubincon',
                'KohlsptsEarned'     :2300,
                'KohlsptsBurned'     :1800,
                'KohlsptsBal'        :500,
                'DaysSinceLastOrder' :31,
                'LastLoginDate'      :'2015-10-30',
                'LastLoginTime'      :'22:38:33'
            });
            $scope.loadSummaryProfile.push({
                'Id'                 :206408023620140602,
                'Name'               :'Mary Rose',
                'KohlsptsEarned'     :9200,
                'KohlsptsBurned'     :3000,
                'KohlsptsBal'        :6200,
                'DaysSinceLastOrder' :203,
                'LastLoginDate'      :'2015-01-22',
                'LastLoginTime'      :'14:18:36'
            });
            $scope.loadSummaryProfile.push({
                'Id'                 :3044454478,
                'Name'               :'James Franco',
                'KohlsptsEarned'     :2400,
                'KohlsptsBurned'     :1000,
                'KohlsptsBal'        :1400,
                'DaysSinceLastOrder' :150,
                'LastLoginDate'      :'2015-03-10',
                'LastLoginTime'      :'12:30:13'
            });
            $scope.loadSummaryProfile.push({
                'Id'                 :3044454548,
                'Name'               :'Matt Hardy',
                'KohlsptsEarned'     :4000,
                'KohlsptsBurned'     :1200,
                'KohlsptsBal'        :2800,
                'DaysSinceLastOrder' :198,
                'LastLoginDate'      :'2015-04-21',
                'LastLoginTime'      :'20:11:33'
            });
            $scope.loadSummaryProfile.push({
                'Id'                 :3044454539,
                'Name'               :'Jason Stratham',
                'KohlsptsEarned'     :4400,
                'KohlsptsBurned'     :1000,
                'KohlsptsBal'        :3400,
                'DaysSinceLastOrder' :500,
                'LastLoginDate'      :'2014-03-24',
                'LastLoginTime'      :'07:20:33'
            });
            $scope.loadSummaryProfile.push({
                'Id'                 :3044454562,
                'Name'               :'Chandler Bing',
                'KohlsptsEarned'     :4500,
                'KohlsptsBurned'     :100,
                'KohlsptsBal'        :4400,
                'DaysSinceLastOrder' :3,
                'LastLoginDate'      :'2015-12-01',
                'LastLoginTime'      :'02:02:33'
            });

            $scope.loadSummaryProfile.push({
                'Id'                 :3044454868,
                'Name'               :'Pheobe Buffay',
                'KohlsptsEarned'     :2000,
                'KohlsptsBurned'     :100,
                'KohlsptsBal'        :1900,
                'DaysSinceLastOrder' :2,
                'LastLoginDate'      :'2015-12-02',
                'LastLoginTime'      :'15:03:33'
            });

            $scope.LatestIteractions.push({
                'Id':206408018820140602,
                'Title':'Request For Return Order #OD12883',
                'Date':'2014-12-01',
                'Type':'Call'
            });
            $scope.LatestIteractions.push({
                'Id':206408018820140602,
                'Title':'Order Status for Order #OD1223',
                'Date':'2014-12-12',
                'Type':'Call'
            });
            $scope.LatestIteractions.push({
                'Id':206408018820140602,
                'Title':'Replacement for Size - #OD2233',
                'Date':'2014-10-21',
                'Type':'Mail'
            });
            $scope.LatestIteractions.push({
                'Id':206408018820140602,
                'Title':'Address Change for Order #OD1122',
                'Date':'2014-11-22',
                'Type':'Mail'
            });
            $scope.LatestIteractions.push({
                'Id':206408018820140602,
                'Title':'Order Status for Order #OD1123',
                'Date':'2014-08-21',
                'Type':'Call'
            });

            $scope.LatestOrders.push({
                'Id':206408018820140602,
                'Title':'Reebok Royal MT Cross Shoes',
                'Qty':'1',
                'Price':'250.39'
            });
            $scope.LatestOrders.push({
                'Id':206408018820140602,
                'Title':'Nike YoungZilla Cap',
                'Qty':'1',
                'Price':'50.29'
            });
            $scope.LatestOrders.push({
                'Id':206408018820140602,
                'Title':'Addidas Neo Sports Shoes',
                'Qty':'2',
                'Price':'349.99'
            });
            $scope.LatestOrders.push({
                'Id':206408018820140602,
                'Title':'Woodland Cruiser Shoes - 10',
                'Qty':'1',
                'Price':'245.99'
            });
            $scope.LatestOrders.push({
                'Id':206408018820140602,
                'Title':'Free Culture- Henley Tee',
                'Qty':'4',
                'Price':'224.99'
            });
        }


        var Workspace = (function () {
            function Workspace(templateUrl, title,disableTab) {
                this.TemplateUrl = templateUrl;
                this.title = title;
                this.disableTab=disableTab;
            }
            return Workspace;
        })();
        function speechToTextCtrl($scope) {
            $scope.Workspaces = new Array();
            $scope.Workspaces.push(new Workspace("templates/speechToText.html", "Speech To Text",false));
            $scope.Workspaces.push(new Workspace("templates/UserProfile.html", "Summary",true));
        }
        speechToTextCtrl($scope);

        $rootScope.loadSummary = function(clickedfile) {
            var audioId=/\d+/.exec(clickedfile);
            $scope.assignDummyData();
            console.log("In Function Load Summary");

            angular.forEach($scope.loadSummaryProfile,function(thisdata) {

                var id1=thisdata.Id;
                console.log("id1"+id1);
                console.log("audID"+audioId);
                if(id1==audioId){
                    $scope.selectedCustomerProfileData.push({
                        'Id'                 :thisdata.Id,
                        'Name'               :thisdata.Name,
                        'KohlsptsEarned'     :thisdata.KohlsptsEarned,
                        'KohlsptsBurned'     :thisdata.KohlsptsBurned,
                        'KohlsptsBal'        :thisdata.KohlsptsBal,
                        'DaysSinceLastOrder' :thisdata.DaysSinceLastOrder,
                        'LastLoginDate'      :thisdata.LastLoginDate,
                        'LastLoginTime'      :thisdata.LastLoginTime
                    });
                }
                else{
                    $scope.selectedCustomerProfileData.push({
                        'Id'                 :thisdata.Id,
                        'Name'               :thisdata.Name,
                        'KohlsptsEarned'     :thisdata.KohlsptsEarned,
                        'KohlsptsBurned'     :thisdata.KohlsptsBurned,
                        'KohlsptsBal'        :thisdata.KohlsptsBal,
                        'DaysSinceLastOrder' :thisdata.DaysSinceLastOrder,
                        'LastLoginDate'      :thisdata.LastLoginDate,
                        'LastLoginTime'      :thisdata.LastLoginTime
                    });
                }

            });




        }

        $rootScope.loadFunnelChart = function(){
            var data = [
                ["Visits",   102],
                ["View Product", 75],
                ["Add to cart",  10],
                ["Buy",        3]
            ];
            var chartF = new D3Funnel("#funnel");
            var options = {
                width: 150,
                height: 100,
                bottomWidth: 1/3,
                bottomPinch: 0,
                isCurved: true,
                curveHeight: 30,
                fillType: "solid",
                isInverted: true,
                hoverEffects: true,
                dynamicArea: true ,
                label: {
                    fontSize: "14px"
                }
            };
            setTimeout(function(){
                chartF.draw(data,options);
            },2000);


        }
        $rootScope.loadFunnelChart();


        $rootScope.hideAll = true;
        $rootScope.hasLanded = false;
        $rootScope.showTable=false;
        $scope.alertShow = false;
        $scope.alertType= "success";
        $scope.alertMessage="Welcome";



        $rootScope.isActive = function (viewLocation)
        {
            return viewLocation === $location.path();
        };


        $rootScope.showLoginModal = function()
        {


        }


    }
    else
    {
        $location.path("/unauthorized");
    }


    /*if($rootScope.hasLanded)
     {




     } else
     {
     $location.path('/');
     }
     */

});

