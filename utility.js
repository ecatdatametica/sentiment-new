(function()
	{

	utility = {};	
utility.randomTime=  function(start, end) 
{
    // get the difference between the 2 dates, multiply it by 0-1, 
    // and add it to the start date to get a new date 
    var diff =  end.getTime() - start.getTime();
    var new_diff = diff * Math.random();
    var date = new Date(start.getTime() + new_diff);
    return date;
}

utility.dataGeneratorTopic = function(startDate,endDate,dataSize)
{
    var sent ={};
    sent.status =false;
    var startValid  = moment(startDate).isValid();
    var endValid  = moment(endDate).isValid();

    if(startValid && endValid)
    {  
    

    //detect if File Exist
    log("Test Data Generator For Topic Analysis");
    log("Start Date : "+startDate);
    log("End   Date : "+endDate);
    log("DataSize   : "+dataSize);
    var depart = ['IT', 'FINANCE', 'MANAGEMENT','R&D','TECHNOLOGY','SYSTEMS'];
    var topicPrimary = ['Password, Registry, Kiosk, Mobile','Product','Fulfillment and Shipping','Promos/Discounts','Basic Order Assistance - No Issues','Hot Issues'];
    
    /*Seconday Topic List*/
    var secondaryBasic = ["Credit / Kohl's Charge Inquiries",'Email Confirmation','Gift Card/KMC/VGC','Modify/Cancel Order (Customer Initiated)','ORDER ASSISTANCE OTHER','Store Assistance','Website Related Errors'];
    var secondaryFulfillment =['BOPUS','Damaged Merchandise','FULFILLMENT and SHIPPING OTHER',"Looking for Cancellation Reason (Kohl's Initiated)",'Missing Items','Received Wrong Items / Wrong Order','Returns','Shipping - Options and Price','Shipping Status - Partial Shipment - Checking Order Status','Shipping Status - Timeframe NOT passed - Checking Order Status',];
    var secondaryHot =['Basic Order Assistance - No Issues'];
    var secondaryPass =['DC3 SUPPORT OTHER','Gift Registry & Lists','Kiosk','Mobile','Password Reset/Login Issues'];  
    var secondaryProduct = ['Product Availability','PRODUCT OTHER','Product Questions']
    var secondaryPromo =["Kohl's Cash - Did Not Receive",'Kohls Cash - Inquiries','Price Matching / Adjustments','Promo Codes','PROMOS/DISCOUNTS OTHER','Rewards - Email / Certificate Inquiries','Rewards - Enrollment Assistance','Rewards - Point Inquiries','Rewards - Profile Update'];
    
    /*3rd Topic List Based On Secondary Topic List*/


    var i =0;
    var List =[];
    while(i!=parseInt(dataSize))
    {
      tuple = {};
      
      tuple.rep_id = parseFloat(Math.floor(Math.random() * 100) + 1);
      tuple.conv_id =  parseFloat(Math.floor(Math.random() * 1000) + 100);
      tuple.dept_id = depart[Math.floor(Math.random()*depart.length)];
      tuple.file_ts =randomTime(new Date(startDate), new Date(endDate));
      tuple.primary_topic = topicPrimary[Math.floor(Math.random()*topicPrimary.length)];


      //based on primary Topic Make Secondary Topic
      if(tuple.primary_topic=='Basic Order Assistance - No Issues')
      {
        //Randomize Data From Secondary Topic For Basic Order Assistance - No Issues
        tuple.secondary_topic = secondaryBasic[Math.floor(Math.random()*secondaryBasic.length)];

      }
      else if(tuple.primary_topic=='Fulfillment and Shipping')
      {
        //Randomize Data From Secondary Topic For Basic Order Assistance - No Issues
        tuple.secondary_topic = secondaryFulfillment[Math.floor(Math.random()*secondaryFulfillment.length)];
      }
      else if(tuple.primary_topic=='Hot Issues')
      {
        //Randomize Data From Secondary Topic For Basic Order Assistance - No Issues
        tuple.secondary_topic = secondaryHot[Math.floor(Math.random()*secondaryHot.length)];
      }
      else if(tuple.primary_topic=='Password, Registry, Kiosk, Mobile')
      {
        //Randomize Data From Secondary Topic For Basic Order Assistance - No Issues
        tuple.secondary_topic = secondaryPass[Math.floor(Math.random()*secondaryPass.length)];
      }
      else if(tuple.primary_topic=='Product')
      {
        //Randomize Data From Secondary Topic For Basic Order Assistance - No Issues
        tuple.secondary_topic = secondaryPass[Math.floor(Math.random()*secondaryProduct.length)];
      }
      else if(tuple.primary_topic=='Promos/Discounts')
      {
        //Randomize Data From Secondary Topic For Basic Order Assistance - No Issues
        tuple.secondary_topic = secondaryPromo[Math.floor(Math.random()*secondaryPromo.length)];
      }


      /*Determine 3rd Topic Based on Secondary Topic*/

      tuple.processed_date =randomTime(new Date(startDate), new Date(endDate));  //Date
      List.push(tuple);

      i+=1;
    }
    sent.status =true;
    sent.data = List;

   }
   else
   {
    log("Date Format Not Valid"); 
   }

    return sent;
}


  utility.uniqueKey = function()
  {
    var keyTemp = [];
    var i=0;
    while(i!=8)
    { 
      keyTemp[i] = Math.floor(Math.random() * 0x10000).toString(16);
      i+=1;
    }
    var key = keyTemp.toString().replace(/,/g,'-');
    return key;
  }


utility.dataGenerator  = function(startDate,endDate,dataSize)
{
    var sent ={};
    sent.status =false;
    var startValid  = moment(startDate).isValid();
    var endValid  = moment(endDate).isValid();

    if(startValid && endValid)
    {  
    

    //detect if File Exist
    log("Test Data Generator For Sentiment Analysis");
    log("Start Date : "+startDate);
    log("End   Date : "+endDate);
    log("DataSize   : "+dataSize);
    var depart = ['IT', 'FINANCE', 'MANAGEMENT','R&D','TECHNOLOGY','SYSTEMS'];
    var i =0;
    var List =[];
    while(i!=parseInt(dataSize))
    {
      tuple = {};
      tuple.Anger =  parseFloat(Math.floor(Math.random() * 100) + 0.5);
      tuple.Anticipation = parseFloat(Math.floor(Math.random() * 100) + 0.5);
      tuple.Disgust =  parseFloat(Math.floor(Math.random() * 100) + 0.5);
      tuple.Fear =  parseFloat(Math.floor(Math.random() * 100) + 0.5);
      tuple.Joy =  parseFloat(Math.floor(Math.random() * 100) + 0.5);
      tuple.Sadness =  parseFloat(Math.floor(Math.random() * 100) + 0.5);
      tuple.Surprise =  parseFloat(Math.floor(Math.random() * 100) + 0.5);
      tuple.Trust =  parseFloat(Math.floor(Math.random() * 100) + 0.5);
      tuple.conv_id =  parseFloat(Math.floor(Math.random() * 1000) + 100);
      tuple.dept_id = depart[Math.floor(Math.random()*depart.length)];
      tuple.file_ts =randomTime(new Date(startDate), new Date(endDate));
      tuple.polnegpercent = parseFloat(Math.floor(Math.random() * 100) + 0.5);
      tuple.polpospercent = parseFloat(Math.floor(Math.random() * 100) + 0.5);
      tuple.processed_date =randomTime(new Date(startDate), new Date(endDate));  //Date
      tuple.rep_id = parseFloat(Math.floor(Math.random() * 100) + 1);
      List.push(tuple);

      i+=1;
    }
    sent.status =true;
    sent.data = List;

   }
   else
   {
    log("Date Format Not Valid"); 
   }

    return sent;
}




utility.FolderReaderMerger = function(path,pathToMerge,cb)
{

  log("Reading File/Folder To Combine : ");
  fs.readdir(path, function(err, audio) 
  {
      fs.readdir(pathToMerge, function(err, audioText) 
      {

         var obj ={};
         global.list.audio =audio;
         global.list.audioText =audioText;

         obj.audio = audio;
         obj.audioText = audioText;

         cb(obj);
        
      });
  });
  log(list);

}


utility.performModelling = function(res) 
{    
       var len = res.length;
       var row_iter = 0;
        var rowCount = 0;
        var arrayList =[];
        var modelArray =[];
        var secondaryArray = [];
        var primaryArray = [];
        var prev_second = "";
        var prev_primary = "";
        var cost_sum=0;
        var secondary_cost_sum =0;
        var primary_groups = {};
    res.forEach(function(val)
    {
       var groups = {};
       var secondary_groups = {}; 
       
      if (prev_second != val.secondary_topic && prev_second != "")
      {
          secondary_groups.category = prev_second;
          secondary_groups.id_category = (rowCount+=1);
          secondary_groups.cost = cost_sum;
          secondary_groups.categories = [];
          secondary_groups.categories=modelArray;
          secondary_cost_sum +=cost_sum;
          secondaryArray.push(secondary_groups);

          if ( prev_primary != val.primary_topic)
          {
            primary_groups.categories = secondaryArray;
            primary_groups.category = prev_primary;
            primary_groups.id_category = (rowCount+=1);
            primary_groups.cost = secondary_cost_sum;
            primaryArray.push(primary_groups);
            primary_groups = {};
            secondaryArray = [];
            secondary_cost_sum = 0;
          }
          cost_sum =0;
          modelArray = [];
      }

        groups.category = val.modelled_topic;
        groups.cost= val.count;
        cost_sum+=val.count;
        groups.id_category = (rowCount+=1);
        modelArray.push(groups); 

      if (row_iter == len-1)
      {
          secondary_groups.category = val.secondary_topic;
          secondary_groups.id_category = (rowCount+=1);
          secondary_groups.cost = cost_sum;
          secondary_groups.categories=modelArray;
          secondary_cost_sum +=cost_sum;
          secondaryArray.push(secondary_groups);

          primary_groups.categories = secondaryArray;
          primary_groups.category = prev_primary;
          primary_groups.id_category = (rowCount+=1);
          primary_groups.cost = secondary_cost_sum;
          primaryArray.push(primary_groups);
      }
      prev_second = val.secondary_topic;
      prev_primary = val.primary_topic;
      row_iter += 1;
    });
    return primaryArray;
};

module.exports = utility;

})();




/*
utility.performModelling = function(res) 
{    
        var groups = {};
        var rowCount = 0;
    res.forEach(function(val)
    {

      if(!groups[val.primary_topic])
      {
            groups[val.primary_topic] = {};
          groups[val.primary_topic]['name'] = val.primary_topic;
          groups[val.primary_topic]['count'] = 0;
          groups[val.primary_topic]['category_id'] = (rowCount+=1);
      }
      if(!groups[val.primary_topic][val.secondary_topic])
        { 
          groups[val.primary_topic][val.secondary_topic] = {};
          groups[val.primary_topic][val.secondary_topic]['name'] = val.secondary_topic;
          groups[val.primary_topic][val.secondary_topic]['count'] = 0;
          groups[val.primary_topic][val.secondary_topic]['category_id'] = (rowCount +=1);
      }
     
      if (!groups[val.primary_topic][val.secondary_topic][val.modelled_topic])
      {
        groups[val.primary_topic][val.secondary_topic][val.modelled_topic] = {};
      }
      //groups[val.secondary_topic]['name'] = val.secondary_topic;
      groups[val.primary_topic][val.secondary_topic]['count'] += val.count;
      groups[val.primary_topic]['count'] += val.count;
      groups[val.primary_topic][val.secondary_topic][val.modelled_topic]['name']  = val.modelled_topic;
      groups[val.primary_topic][val.secondary_topic][val.modelled_topic]['count']  = val.count;
      groups[val.primary_topic][val.secondary_topic][val.modelled_topic]['category_id'] = (rowCount +=1);

     // groups[val.primary_topic][val.secondary_topic] [val.modelled_topic] = groups["all"][val.modelled_topic] = 1;
    });


*/