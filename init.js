(function()
	{
		
		message ={};
		global.list ={};
		var utility = require("./utility");
		moment  = require("moment");
		express = require("express");
		global.app = express();
		mysql  = require('mysql');
		bodyParser = require("body-parser");
		http = require("http");
		io = require("socket.io")(http);
		fs = require('fs');
		path = require("path");
	    morgan = require('morgan');
		jwt = require("jsonwebtoken");
		var keys = utility.uniqueKey();
		log("Unique Key : "+keys);
		secret = keys;

		


	
//CORS middleware
/*var allowCrossDomain = function(req, res, next) 
{
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    next();
    log("Enabling CORS");
}
*/
   app.set('superSecret',secret); // secret variable
   allowCrossDomain = function(req, res,next) 
    {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type, Authorization');
    next();
    }


	ensureAuthorization = function(req,res,next)
	{
		var bearerHeader = req.param("token")||req.body.token || req.query.token || req.headers['x-access-token']||req.headers["authorization"];
	    log("Token : ");
	    log(bearerHeader);
	    if(typeof bearerHeader !== 'undefined') 
	    {
	         if(bearerHeader) 
	         {
			    // verifies secret and checks exp
			    jwt.verify(bearerHeader, app.get('superSecret'), function(err, decoded) 
			    {      
			      if (err) {
			        return res.json({ success: false, message: 'Failed to authenticate token.' });    
			      } else {
			        // if everything is good, save to request for use in other routes
			        req.decoded = decoded;    
			        next();
			      }
			    });

		  }else 
		  {

		    // if there is no token
		    // return an error
		    return res.status(403).send(
		    { 
		        success: false, 
		        message: 'No token provided.' 
		    });
		    
		  }
	    } 
	    else 
	    {
	        res.send(403);
	    }

	  
	}





	//Here we are configuring express to use body-parser as middle-ware.
	app.use(bodyParser.urlencoded({ extended: true }));
	app.use(bodyParser.json());
	app.use(allowCrossDomain);
	/*app.use(morgan('combined'))*/


	/*PORT AND BASIC CONFIG*/
	app.set("PORT",process.env.PORT||3000);

	/*SET PATH FOR STATIC FOLDER FOR DATA TRANSMISSION*/
	app.use(express.static(__dirname+"/PUBLIC"));

	//Set Audio/text/Files PATH
	app.set("audioPath",__dirname+"/PUBLIC/Data/Data/Wav/");
	app.set("audioText",__dirname+"/PUBLIC/Data/Data/Transcripts/")


	/*Init DAtabase*/

global.user ={};
user.isAuth =false;
user.isDB = false;



/*Connect To Db*/
	global.connection = mysql.createConnection({
	  host     : 'localhost',
	  user     : 'sa_user',
	  password : 'sa_user',
	  database : 'sentiments'
	  /*dateStrings :true,
	  connectTimeout : 2000*/
	});
	/*
		global.connection = mysql.createConnection({
	  host     : '10.200.99.110',
	  user     : 'sa_user',
	  password : 'sa_user',
	  database : 'sentiments',
	  dateStrings :true,
	  connectTimeout : 2000
	});

	*/




	connection.on('error', function(err) {
	   user.isDB = false;
	   log(err.code); // 'ER_BAD_DB_ERROR'
	});

	connection.connect(function(err) 
	{
	   
	 
	   
	 // log(err.code); // 'ECONNREFUSED'
	 // log(err.fatal);
	  log("Please Wait Connecting.....");
	  if(err)
	   { 
	    log("Connection Error");
	    log("SQL Db Not Reachable...Simulating Data On Request");
	    log("Default To Test JSON Data : Starting AS Dummy Server");
	    log("-----------------------------------------------------");
	    connection.end();
	   }
	   else
	   {
	    user.isDB = true;
	    log("SQL Db .....Initilizing Server Started With ALL Real Route/Data ");
	    log("DB Connected....");
	    log("-----------------------------------------------------");
	   }

	});


})();
