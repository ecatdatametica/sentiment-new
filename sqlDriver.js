var mysql  = require('mysql');
var DB = {};


DB.connect = function(host,username,password,success,failure)
{
   try
   {

	var connection = mysql.createConnection({
  	host     : host,
  	user     : username,
  	password : password
	});

	if(connection)
	{
		
		log("DB Connected");
		success(connection);
		return connection;
		
	}
	else
	{
		failure(false);
		return false;
	}

	
	return this;
  }
  catch(e)
  {
  	failure(e);
  }


};




module.exports  = DB;